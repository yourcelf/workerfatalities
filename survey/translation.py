from modeltranslation.translator import translator, TranslationOptions
from survey.models import Occupation, Affiliation, Exposure, SiteCopy

class NameOnlyFieldTranslationOptions(TranslationOptions):
    fields = ('name',)


for model in (Occupation, Affiliation, Exposure):
    translator.register(model, NameOnlyFieldTranslationOptions)

class SiteCopyTranslationOptions(TranslationOptions):
    fields = ('description',)
translator.register(SiteCopy, SiteCopyTranslationOptions)
