import os
import json
import datetime

from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.db import connection
from django.utils import timezone
from survey.models import *
from survey.aggregation import * 
from survey.utils import parse_rows, PendingEntryJSONEncoder
from survey.forms import EntryConfirmationForm

class PendingEntryTest(TestCase):
    maxDiff = 10000
    def setUp(self):
        rows = [[
            {'row': 1, 'value': 42178.0, 'col': 0, 'type': 3, 'heading': 'today'},
            {'row': 1, 'value': 'Walter', 'col': 1, 'type': 1, 'heading': 'first_name'},
            {'row': 1, 'value': 'Darden', 'col': 2, 'type': 1, 'heading': 'last_name'},
            {'row': 1, 'value': 64.0, 'col': 3, 'type': 2, 'heading': 'age'},
            {'row': 1, 'value': 'male', 'col': 4, 'type': 1, 'heading': 'sex'},
            {'row': 1, 'value': 42174.0, 'col': 5, 'type': 3, 'heading': 'date_event'},
            {'row': 1, 'value': 42174.0, 'col': 6, 'type': 3, 'heading': 'date_death'},
            {'row': 1, 'value': "longtime Camp Courant maintenance worker who collapsed on the job Friday died of anaphylactic shock from bee stings, the state medical examiner's office said Monday. He was stung by yellow jacket bees that had been inside an air conditioner.", 'col': 7, 'type': 1, 'heading': 'event'},
            {'row': 1, 'value': 'Farmington', 'col': 8, 'type': 1, 'heading': 'event_city'},
            {'row': 1, 'value': 'CT', 'col': 9, 'type': 1, 'heading': 'event_state'},
            {'row': 1, 'value': 'http://www.courant.com/community/farmington/hc-farmington-camp-courant-death-0623-20150622-story.html', 'col': 10, 'type': 1, 'heading': 'source_1'},
            {'row': 1, 'value': 'Maintenance worker', 'col': 11, 'type': 1, 'heading': 'occupation'},
            {'row': 1, 'value': '', 'col': 12, 'type': 0, 'heading': 'industry'},
            {'row': 1, 'value': '', 'col': 13, 'type': 0, 'heading': 'immigr'},
            {'row': 1, 'value': '', 'col': 14, 'type': 0, 'heading': 'origin_county'},
            {'row': 1, 'value': 'temp', 'col': 15, 'type': 1, 'heading': 'affil'},
            {'row': 1, 'value': 0, 'col': 16, 'type': 4, 'heading': 'affil/union'},
            {'row': 1, 'value': 1, 'col': 17, 'type': 4, 'heading': 'affil/temp'},
            {'row': 1, 'value': 0, 'col': 18, 'type': 4, 'heading': 'affil/day'},
            {'row': 1, 'value': 0, 'col': 19, 'type': 4, 'heading': 'affil/guest'},
            {'row': 1, 'value': 0, 'col': 20, 'type': 4, 'heading': 'affil/public'},
            {'row': 1, 'value': 0, 'col': 21, 'type': 4, 'heading': 'affil/contractor'},
            {'row': 1, 'value': 'violence', 'col': 22, 'type': 1, 'heading': 'exposure'},
            {'row': 1, 'value': 1, 'col': 23, 'type': 4, 'heading': 'exposure/violence'},
            {'row': 1, 'value': 0, 'col': 24, 'type': 4, 'heading': 'exposure/transportation'},
            {'row': 1, 'value': 0, 'col': 25, 'type': 4, 'heading': 'exposure/fires'},
            {'row': 1, 'value': 0, 'col': 26, 'type': 4, 'heading': 'exposure/falls'},
            {'row': 1, 'value': 0, 'col': 27, 'type': 4, 'heading': 'exposure/exposure'},
            {'row': 1, 'value': 0, 'col': 28, 'type': 4, 'heading': 'exposure/contact'},
            {'row': 1, 'value': 0, 'col': 29, 'type': 4, 'heading': 'exposure/overexertion'},
            {'row': 1, 'value': 0, 'col': 30, 'type': 4, 'heading': 'exposure/other'},
            {'row': 1, 'value': 0, 'col': 31, 'type': 4, 'heading': 'exposure/unknown'},
            {'row': 1, 'value': '', 'col': 32, 'type': 0, 'heading': 'origin_city'},
            {'row': 1, 'value': '', 'col': 33, 'type': 0, 'heading': 'origin_state'},
            {'row': 1, 'value': 'Camp Courant', 'col': 34, 'type': 1, 'heading': 'direct_employer'},
            {'row': 1, 'value': '', 'col': 35, 'type': 0, 'heading': 'contract_1'},
            {'row': 1, 'value': '', 'col': 36, 'type': 0, 'heading': 'contract_2'},
            {'row': 1, 'value': '', 'col': 37, 'type': 0, 'heading': 'source_2'},
            {'row': 1, 'value': '', 'col': 38, 'type': 0, 'heading': 'source_3'},
            {'row': 1, 'value': '', 'col': 39, 'type': 0, 'heading': 'photo'},
            {'row': 1, 'value': 'Bethany', 'col': 40, 'type': 1, 'heading': 'collector'}
        ]]
        self.pe = parse_rows(rows)[0]

class TestPendingEntryParsingAndSerialization(PendingEntryTest):
    def test_serialization(self):
        expected = {
          "fields": {
            'affiliation': {
              'cell': {'col': 15, 'heading': 'affil', 'row': 1, 'type': 1, 'value': 'temp'},
              'errors': ['Not an existing value for this field'],
              'value': 'temp'
            },
            'age': {
              'cell': {'col': 3, 'heading': 'age', 'row': 1, 'type': 2, 'value': 64.0},
              'errors': [],
              'value': 64
            },
            'collector': {
              'cell': {'col': 40, 'heading': 'collector', 'row': 1, 'type': 1, 'value': 'Bethany'},
              'errors': ['Not an existing value for this field'],
              'value': 'Bethany'
            },
            'date_created': {
              'cell': {'col': 0, 'heading': 'today', 'row': 1, 'type': 3, 'value': 42178.0},
              'errors': [],
              'value': timezone.make_aware(datetime.datetime(2015, 6, 23))
            },
            'date_death': {
              'cell': {'col': 6, 'heading': 'date_death', 'row': 1, 'type': 3, 'value': 42174.0},
              'errors': [],
              'value': datetime.date(2015, 6, 19)
            },
            'date_event': {
              'cell': {'col': 5, 'heading': 'date_event', 'row': 1, 'type': 3, 'value': 42174.0},
              'errors': [],
              'value': datetime.date(2015, 6, 19)
            },
            'direct_employer': {
              'cell': {'col': 34, 'heading': 'direct_employer', 'row': 1, 'type': 1, 'value': 'Camp Courant'},
              'errors': [],
              'value': Employer.objects.get(name="Camp Courant")
            },
            'event': {
              'cell': {'col': 7, 'heading': 'event', 'row': 1, 'type': 1,
                       'value':
                         'longtime Camp Courant maintenance worker who '
                         'collapsed on the job Friday died of '
                         'anaphylactic shock from bee stings, the '
                         "state medical examiner's office said Monday. "
                         'He was stung by yellow jacket bees that had '
                         'been inside an air conditioner.'},
              'errors': [],
              'value': 'longtime Camp Courant maintenance worker who collapsed '
                       'on the job Friday died of anaphylactic shock from bee '
                       "stings, the state medical examiner's office said "
                       'Monday. He was stung by yellow jacket bees that had '
                       'been inside an air conditioner.'
            },
            'event_city': {
              'cell': {'col': 8, 'heading': 'event_city', 'row': 1, 'type': 1, 'value': 'Farmington'},
              'errors': [],
              'value': 'Farmington'
            },
            'event_state': {
              'cell': {'col': 9, 'heading': 'event_state', 'row': 1, 'type': 1, 'value': 'CT'},
              'errors': [],
              'value': 'CT'
            },
            'exposure': {
              'cell': {'col': 22, 'heading': 'exposure', 'row': 1, 'type': 1, 'value': 'violence'},
              'errors': ['Not an existing value for this field'],
              'value': 'violence'
            },
            'first_name': {
              'cell': {'col': 1, 'heading': 'first_name', 'row': 1, 'type': 1, 'value': 'Walter'},
              'errors': [],
              'value': 'Walter'
            },
            'immigrant': {
              'cell': {'col': 13, 'heading': 'immigr', 'row': 1, 'type': 0, 'value': ''},
              'errors': [],
              'value': None
            },
            'industry': {
              'cell': {'col': 12, 'heading': 'industry', 'row': 1, 'type': 0, 'value': ''},
              'errors': [],
              'value': None
            },
            'last_name': {
              'cell': {'col': 2, 'heading': 'last_name', 'row': 1, 'type': 1, 'value': 'Darden'},
              'errors': [],
              'value': 'Darden'
            },
            'occupation': {
              'cell': {'col': 11, 'heading': 'occupation', 'row': 1, 'type': 1, 'value': 'Maintenance worker'},
              'errors': [],
              'value': Occupation.objects.get(name='Maintenance worker')
            },
            'origin_city': {
              'cell': {'col': 32, 'heading': 'origin_city', 'row': 1, 'type': 0, 'value': ''},
              'errors': [],
              'value': ''
            },
            'origin_country': {
              'cell': {'col': 14, 'heading': 'origin_county', 'row': 1, 'type': 0, 'value': ''},
              'errors': [],
              'value': None
            },
            'origin_state': {
              'cell': {'col': 33, 'heading': 'origin_state', 'row': 1, 'type': 0, 'value': ''},
              'errors': [],
              'value': ''
            },
            'photo': {
              'cell': {'col': 39, 'heading': 'photo', 'row': 1, 'type': 0, 'value': ''},
              'errors': [],
              'value': None
            },
            'sex': {
              'cell': {'col': 4, 'heading': 'sex', 'row': 1, 'type': 1, 'value': 'male'},
              'errors': [],
              'value': 'male'
            },
          },
          "m2m": {
            'sources': [{
              'cell': {'col': 10, 'heading': 'source_1', 'row': 1, 'type': 1, 'value': 'http://www.courant.com/community/farmington/hc-farmington-camp-courant-death-0623-20150622-story.html'},
              'errors': [],
              'value': Source.objects.get(url='http://www.courant.com/community/farmington/hc-farmington-camp-courant-death-0623-20150622-story.html')
            }]
          },
          "duplicate_action": {},
          "potential_duplicates": [],
          "row": 1
        }

        out = self.pe.to_dict()
        # big dictionary comparsion gets unwieldy with diffs.  Break it up to
        # get a little easier to check errors.
        top = set(('fields', 'm2m', 'potential_duplicates', 'row', 'duplicate_action'))
        self.assertEquals(set(out.keys()), top)
        for key in ('fields', 'm2m'):
            self.assertEquals(set(out[key].keys()), set(expected[key].keys()))
            for subkey in out[key]:
                self.assertEquals(out[key][subkey], expected[key][subkey])
        self.assertEquals(set(out['potential_duplicates']), 
                          set(expected['potential_duplicates']))

        # Vivify and re-serialize.
        new_pe = PendingEntry.from_dict(out)
        new_out = new_pe.to_dict()
        self.assertEquals(new_out, expected)

    def test_value_replacement_simple(self):
        # Unknown key
        self.assertRaises(Exception, lambda: self.pe.replace_value("fiddlesticks", 105))
        # Simple value replacement
        self.pe.replace_value("age", 103)
        self.assertEquals(self.pe.fields['age'], {
            'cell': {'col': 3, 'heading': 'age', 'row': 1, 'type': 2, 'value': 64.0},
            'errors': [],
            'value': 103
        })

    def test_value_replacement_foreign_key_by_id(self):
        # Foreign key relation by id
        occ = Occupation.objects.get_or_create(name='Maintenance worker')[0]
        self.assertRaises(Exception, lambda: self.pe.replace_value("occupation", "asdf"))
        self.pe.replace_value("occupation", occ.id)
        self.assertEquals(self.pe.fields['occupation'], {
            'cell': {'col': 11, 'heading': 'occupation', 'row': 1, 'type': 1, 'value': 'Maintenance worker'},
            'errors': [],
            'value': occ
        })

    def test_value_replacement_foreign_key_by_model(self):
        occ2 = Occupation.objects.create(name="Workin' on the Railroad")
        self.pe.replace_value("occupation", occ2)
        self.assertEquals(self.pe.fields['occupation'], {
            'cell': {'col': 11, 'heading': 'occupation', 'row': 1, 'type': 1, 'value': 'Maintenance worker'},
            'errors': [],
            'value': occ2
        })

    def test_value_replacement_none(self):
        self.pe.replace_value("occupation", None)
        self.assertEquals(self.pe.fields['occupation'], {
            'cell': {'col': 11, 'heading': 'occupation', 'row': 1, 'type': 1, 'value': 'Maintenance worker'},
            'errors': [],
            'value': None
        })

    def test_value_replacement_m2m(self):
        old_source = self.pe.m2m['sources'][0]['value']
        new_source = Source.objects.create(note="Fun times")
        # By id
        self.pe.replace_value("sources", new_source.id, old_source)
        self.assertEquals(self.pe.m2m['sources'], [{
            'cell': {'col': 10, 'heading': 'source_1', 'row': 1, 'type': 1, 'value': 'http://www.courant.com/community/farmington/hc-farmington-camp-courant-death-0623-20150622-story.html'},
            'errors': [],
            'value': new_source
        }])

        # By model (switch back to old source)
        self.pe.replace_value("sources", old_source, new_source)
        self.assertEquals(self.pe.m2m['sources'], [{
            'cell': {'col': 10, 'heading': 'source_1', 'row': 1, 'type': 1, 'value': 'http://www.courant.com/community/farmington/hc-farmington-camp-courant-death-0623-20150622-story.html'},
            'errors': [],
            'value': old_source
        }])

    def test_validation(self):
        def _get_errors():
            errors = {}
            for name, field_dict in self.pe.fields.items():
                errors[name] = field_dict['errors'][:]
            return errors

        # Revalidation should preserve any existing errors.
        known_errors = _get_errors()
        self.pe.revalidate()
        self.assertEquals(known_errors, _get_errors())

        # Introduce an error.
        self.pe.replace_value('date_event', '20155-11-11')
        self.pe.revalidate()
        known_errors['date_event'] = [
            "'20155-11-11' value has an invalid date format. "
            "It must be in YYYY-MM-DD format."
        ]
        self.assertEquals(_get_errors(), known_errors)

class TestUpdateEntry(PendingEntryTest):
    def test_update_entry(self):
        admin = User.objects.create_user(username="admin", password="admin")
        admin.is_superuser = True
        admin.is_staff = True
        admin.save()
        session = self.client.session
        session['pending_entries_json'] = json.dumps([self.pe.to_dict()], cls=PendingEntryJSONEncoder)
        session.save()
        self.client.login(username="admin", password="admin")
        res = self.client.post(reverse("update_pending_spreadsheet_entry"), {
            "row": "1",
            "col": "11",
            "action": "add",
            "field": "occupation",
            "value": "Some new occupation"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 200)
        obj = json.loads(res.content.decode('utf-8'))
        pe_data = json.loads(self.client.session['pending_entries_json'])
        self.assertEquals(pe_data[0]['fields']['occupation']['value']['name'],
            'Some new occupation'
        )

class TestEntryConfirmationForm(PendingEntryTest):
    def test_confirmation_form(self):
        WorkerFatality.objects.create(
            first_name=self.pe.fields['first_name']['value'],
            last_name=self.pe.fields['last_name']['value'],
            date_event=self.pe.fields['date_event']['value'],
            date_death=self.pe.fields['date_death']['value'])

        self.assertEquals(
            WorkerFatality.objects.find_potential_duplicates(self.pe, max_dist=1000, empty_penalty=0).count(),
            1
        )

        # Ignore any errors, so that we trigger output of duplicates
        self.pe.revalidate()
        for field, desc in self.pe.fields.items():
            desc['errors'] = []
           

        form = EntryConfirmationForm(entries=[self.pe])
        dup_dict = form.duplicates_dict()
        self.assertEquals(dup_dict, {
            'heading': [
                'id', 'first_name', 'last_name', 'date_event', 'date_death',
                'event', 'event_city', 'event_state', 'score', 'admin_url'
            ],
            'rows': {'1': {
                'entry': [
                    '', 'Walter', 'Darden',
                    datetime.date(2015, 6, 19), datetime.date(2015, 6, 19),
                    "longtime Camp Courant maintenance worker who collapsed on the job Friday died of anaphylactic shock from bee stings, the state medical examiner's office said Monday. He was stung by yellow jacket bees that had been inside an air conditioner.",
                    'Farmington', 'CT', '', ''],
                'duplicates': [
                    [1, 'Walter', 'Darden',
                        datetime.date(2015, 6, 19), datetime.date(2015, 6, 19),
                        '', '', '', 1.78885438199983, '/admin/survey/workerfatality/1/']
                ]
            }}
        })


class TestPotentialDuplicates(TestCase):
    def setUp(self):
        kwargs = {"date_event": "2015-11-11", "date_death": "2015-11-11"}
        john = WorkerFatality.objects.create(
                first_name="John", last_name="Dough", event_city="Dallas",
                sex="male",
                **kwargs)
        joe = WorkerFatality.objects.create(
                first_name="Joe", last_name="Miller", sex="male", **kwargs)
        sue = WorkerFatality.objects.create(
                first_name="Sue", sex="female", **kwargs)

    def test_similarity(self):
        qs = WorkerFatality.objects.only("first_name").annotate(
            first_name_similarity=SimilarityDist("first_name", "John")
        )
        for wf in qs:
            if wf.first_name == "John":
                self.assertEquals(wf.first_name_similarity, 0)
            if wf.first_name == "Joe":
                self.assertEquals(wf.first_name_similarity, 1 - 0.285714)
            if wf.first_name == "Sue":
                self.assertEquals(wf.first_name_similarity, 1)

    def test_product(self):
        qs = WorkerFatality.objects.annotate(score=Product(3, 2))
        for wf in qs:
            self.assertEquals(wf.score, 6)

    def test_date_diff(self):
        qs = WorkerFatality.objects.annotate(date_diff=DateDiff("date_event", "2015-11-23"))
        for wf in qs:
            self.assertEquals(wf.date_diff, 12)

    def test_conditional_weighted_average(self):
        qs = WorkerFatality.objects.annotate(score=ConditionalWeightedAverage((
            ("first_name", SimilarityDist("first_name", "John"), 2.0),
            ("last_name", SimilarityDist("last_name", "Dough"), 0.5),
            ("event_city", SimilarityDist("event_city", "Dulles"), 1.0)
        )))
        for wf in qs:
            if wf.first_name == "John":
                self.assertEquals(wf.score, 0.307692309220632)

    def test_conditional_weighted_distance(self):
        qs = WorkerFatality.objects.annotate(score=ConditionalWeightedDistance(
            (("first_name", [""], SimilarityDist("first_name", "John"), 2.0),
             ("last_name", [""], SimilarityDist("last_name", "Dough"), 5)),
            penalty=0))

        for wf in qs:
            if wf.first_name == "John":
                self.assertEquals(wf.score, 0)
            if wf.first_name == "Joe":
                self.assertEquals(wf.score, 5.20007850170017)
            if wf.first_name == "Sue":
                self.assertEquals(wf.score, 2)

        qs = WorkerFatality.objects.annotate(score=ConditionalWeightedDistance(
            (("first_name", [""], SimilarityDist("first_name", "John"), 2.0),
             ("last_name", [""], SimilarityDist("last_name", "Dough"), 5)),
            penalty=0.5))

        for wf in qs:
            if wf.first_name == "John":
                self.assertEquals(wf.score, 0)
            if wf.first_name == "Joe":
                self.assertEquals(wf.score, 5.20007850170017)
            if wf.first_name == "Sue":
                self.assertEquals(wf.score, 2.12132034355964)

    def test_find_potential_duplicates(self):
        pe = PendingEntry()
        pe.fields['first_name'] = {'value': "John"}
        pe.fields['last_name'] = {'value': "Dough"}
        pe.fields['sex'] = {'value': "male"}
        pe.fields['age'] = {'value': 32}

        
        # Should ignore empty age because fields are null in fixture.
        qs = WorkerFatality.objects.find_potential_duplicates(pe, max_dist=1000,
                empty_penalty=0)
        self.assertEquals(
            [(m.first_name, m.score) for m in qs],
            [("John", 0), ("Joe", 2.12372411248756), ("Sue", 5.09901951359278)]
        )

        # Will penalize for null age.
        qs = WorkerFatality.objects.find_potential_duplicates(pe, max_dist=1000,
                empty_penalty=0.5)
        self.assertEquals(
            [(m.first_name, m.score) for m in qs],
            [('John', 0.707106781186548), ('Joe', 2.2383485220048), ('Sue', 5.19615242270663)]
        )

    def test_agg_as_python(self):
        pass

class TestSpreadsheetUpload(TestCase):
    def test_spreadsheet_upload(self):
        filename = os.path.join(
            os.path.dirname(__file__), "test_data", "test_spreadsheet.xlsx"
        )
        with open(filename, 'rb') as fh:
            res = self.client.post(reverse("spreadsheet"), {
                'name': 'test_spreadsheet.xlsx', 'attachment': fh
            })
            self.assertEquals(res.status_code, 302)
