import urllib

from django.db import models, DatabaseError
from django.db.models import Func
from django.core.exceptions import ValidationError
from django.core.serializers.json import DjangoJSONEncoder
from django.core.urlresolvers import reverse
from django.core.validators import URLValidator
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.utils.timezone import now
from localflavor.us.models import USPostalCodeField
from locality.models import Country
from naics.models import NAICSCode
from django.utils.translation import ugettext_lazy as _
from collections import defaultdict
from jsonfield import JSONField

import survey.aggregation as agg

class NameOnlyField(models.Model):
    name = models.CharField(max_length=255, verbose_name=_("name"), unique=True)

    def to_dict(self):
        return {"id": self.id, "name": self.name }

    def __str__(self):
        return self.name

    class Meta:
        abstract = True
        ordering = ['name']

class Occupation(NameOnlyField):
    class Meta(NameOnlyField.Meta):
        verbose_name = _("occupation")
        verbose_name_plural = _("occupations")

class Affiliation(NameOnlyField):
    class Meta(NameOnlyField.Meta):
        verbose_name = _("affiliation")
        verbose_name_plural = _("affiliations")

class Exposure(NameOnlyField):
    class Meta(NameOnlyField.Meta):
        verbose_name = _("exposure")
        verbose_name_plural = _("exposures")

class Employer(NameOnlyField):
    class Meta(NameOnlyField.Meta):
        verbose_name = _("employer")
        verbose_name_plural = _("employers")

class ContractManager(models.Manager):
    def build_from_value(self, value):
        if value is None:
            return None
        name = str(value).strip()
        if len(name) > 255:
            raise ValueError("'{}' too long. Maximum length {}.".format(
                name, 255))
        try:
            return self.get(name=name)
        except Contract.DoesNotExist:
            return Contract(name=name)
        
class Contract(NameOnlyField):
    objects = ContractManager()
    class Meta(NameOnlyField.Meta):
        verbose_name = _("contract")
        verbose_name_plural = _("contracts")

class Collector(NameOnlyField):
    class Meta(NameOnlyField.Meta):
        verbose_name = _("collector")
        verbose_name_plural = _("collectors")

class SourceManager(models.Manager):
    def build_from_value(self, value):
        if value:
            value = str(value).strip()
            parse_result = urllib.parse.urlparse(value)
            if parse_result.scheme and parse_result.netloc:
                try:
                    return self.get(url=value)
                except Source.DoesNotExist:
                    return Source(url=value)
            else:
                if len(value) > 255:
                    raise ValueError("'{}' too long. Maximum length {}.".format(
                        value, 255))
                try:
                    return self.get(note=value)
                except Source.DoesNotExist:
                    return Source(note=value)
        return None

class Source(models.Model):
    url = models.TextField(validators=[URLValidator()], blank=True,
            verbose_name=_("URL"), default="")
    note = models.CharField(blank=True, max_length=255, verbose_name=("note"),
            default="")

    objects = SourceManager()

    def to_dict(self):
        return {
            "id": self.id,
            "url": self.url,
            "note": self.note
        }

    def __str__(self):
        return self.note or self.url

    class Meta:
        verbose_name = _("source")
        verbose_name_plural = _("sources")

class WorkerFatalityManager(models.Manager):
    def find_potential_duplicates(self, entry, max_dist=2, empty_penalty=0.2):
        # Establish a function to get attributes from the entry.  The entry
        # could be a WorkerFatality instance or a not-yet-saved PendingEntry
        # instance.
        if isinstance(entry, WorkerFatality):
            getter = lambda v: getattr(entry, v)
            nonduplicates = list(entry.nonduplicates.all().values_list('pk', flat=True))
            nonduplicates.append(entry.pk)
        elif isinstance(entry, PendingEntry):
            getter = lambda v: entry.fields.get(v, {}).get('value', None)
            # If we have a pending entry with an already-defined duplicate
            # action, return no further duplicates.
            if 'overwrite' in entry.duplicate_action:
                return self.none()
            elif 'nonduplicates' in entry.duplicate_action:
                nonduplicates = entry.duplicate_action['nonduplicates']
            else:
                nonduplicates = []

        # 1. Ensure that there is some minimum set of fields entered.
        #    Since we only consider non-empty fields, this avoids a mostly
        #    empty entry from matching against everything.
        minima = [
            ("last_name",),
            ("date_death",),
            ("event_state",),
        ]
        for minimum in minima:
            if all([getter(m) not in [None, ""] for m in minimum]):
                break
        else:
            # None of the minimum criteria were matched; return empty list.
            return self.none()

        # 2. Accumulate a similarity score. Only consider fields that both
        #    the pending entry and the database have. add_expr_if_exists
        #    filters on the entry side; ConditionalWeightedDistance filters on
        #    the database side.
        def add_expr_if_exists(field_func_weights):
            out = []
            for field_name, empties, func, weight in field_func_weights:
                val = getter(field_name)
                if val:
                    out.append((field_name, empties, func(field_name, val), weight))
            return out

        qs = WorkerFatality.objects.all()
        if nonduplicates:
            qs = qs.exclude(pk__in=nonduplicates)

        qs = qs.select_related()
        qs = qs.annotate(
            score=agg.ConditionalWeightedDistance(add_expr_if_exists([
                ("first_name", [""], lambda f,v: agg.SimilarityDist(f,v), 1),
                ("last_name", [""], lambda f,v: agg.SimilarityDist(f,v), 2),
                ("event_city", [""], lambda f,v: agg.SimilarityDist(f,v), 2),
                ("event_state", [""], lambda f,v: agg.ZeroIfEqual(f,v), 2),
                ("sex", [""], lambda f,v: agg.ZeroIfEqual(f,v), 5),
                ("age", [None], lambda f,v: agg.AbsDiff(f,float(v)), 0.1),
                ("immigrant", [None], lambda f,v: agg.ZeroIfEqual(f,v), 1),
                ("origin_city", [""], lambda f,v: agg.SimilarityDist(f,v), 1),
                ("origin_state", [""], lambda f,v: agg.ZeroIfEqual(f,v), 1),
                ("origin_country", [None], lambda f,v: agg.ZeroIfEqual(f,v), 1),
                ("date_event", [None], lambda f,v: agg.DateDiff(f,v), 0.5),
                ("date_death", [None], lambda f,v: agg.DateDiff(f,v), 0.5),
                #("industry", lambda f,v: agg.ZeroIfEqual(... ancestor ...), 1)
            ]), penalty=empty_penalty),
        ).order_by("score")
        qs = qs.filter(score__lte=max_dist)
        return qs

class WorkerFatality(models.Model):
    date_created = models.DateTimeField(
        default=now, verbose_name=_("date created"))
    date_modified = models.DateTimeField(
        auto_now=True, verbose_name=_("date modified"))
    first_name = models.CharField(
        max_length=255, blank=True, verbose_name=_("Deceased worker's first name"))
    last_name = models.CharField(
        max_length=255, blank=True, verbose_name=_("Deceased worker's last name"))
    age = models.IntegerField(
        null=True, blank=True, verbose_name=_("Deceased worker's age"))
    sex = models.CharField(max_length=255,
        choices=(("male", _("male")), ("female", _("female"))),
        blank=True,
        verbose_name=_("Deceased worker's sex"))
    date_event = models.DateField(verbose_name=_("date of event"),
        help_text=_("When did the event which resulted in the worker's death occur?"),
        blank=True, null=True)
    date_death = models.DateField(
        verbose_name=_("date of death"),
        help_text=_("When did the worker die?"),
        blank=True, null=True)
    event = models.TextField(verbose_name=_("event description"),
        help_text=_("Please describe the event in detail."), blank=True)
    event_city = models.CharField(
        max_length=255, verbose_name=_("event city"),
        blank=True)
    event_state = USPostalCodeField(verbose_name=_("event state"),
        blank=True)
    occupation = models.ForeignKey(Occupation,
        blank=True, null=True, verbose_name=_("Worker's occupation"))
    industry = models.ForeignKey(
        NAICSCode, blank=True, null=True, on_delete=models.SET_NULL,
        verbose_name=_("Industry"),
        help_text=_("Please choose an industry code that best corresponds to the worker's occupation."))
    immigrant = models.NullBooleanField(
        default=None, verbose_name=_("Was the worker an immigrant?"))
    affiliation = models.ForeignKey(
        Affiliation, on_delete=models.SET_NULL, blank=True, null=True,
        verbose_name=_("Deceased worker's affiliation"))
    exposure = models.ForeignKey(
        Exposure, on_delete=models.SET_NULL, blank=True, null=True,
        verbose_name=_("Risk worker was exposed to"))

    origin_country = models.ForeignKey(
        Country, on_delete=models.SET_NULL, blank=True, null=True,
        verbose_name=_("Worker's origin country"))
    origin_city = models.CharField(
        max_length=255, blank=True, verbose_name=_("Worker's origin city"))
    origin_state = models.CharField(
        max_length=255,
        verbose_name=_("Worker's origin state"), blank=True)

    direct_employer = models.ForeignKey(
        Employer, on_delete=models.SET_NULL, blank=True, null=True,
        verbose_name=_("Worker's direct employer"),
        help_text=_("Who was paying the worker?"))
    contracts = models.ManyToManyField(
        Contract, blank=True, verbose_name=_("Contractors of the worker's employer"),
        help_text=_("Was the worker's employer contracted by another company? Leave blank if unknown."))

    photo = models.ImageField(
        upload_to='images/%Y/%m/%d', blank=True, null=True,
        verbose_name=_("Photo of deceased"))

    collector = models.ForeignKey(
        Collector, on_delete=models.SET_NULL, blank=True, null=True,
        verbose_name=_("collector of this record"))

    sources = models.ManyToManyField(
        Source, blank=True, verbose_name=_("Sources of information about worker"))

    notes = models.TextField(
        verbose_name=_("Additional information"),
        help_text=_("Please include any additional comments or info about this."),
        blank=True)

    email = models.EmailField(
        blank=True, null=True,
        verbose_name=_("Email"),
        help_text=_("(Optional) Please include an email we can contact if we need further information."))

    # Other WorkerFatality objects that should not be included as potential
    # duplicates of this one.
    nonduplicates = models.ManyToManyField('self', blank=True,
        verbose_name=_("Non-duplicates"),
        help_text=_("These other entries have been ruled out as duplicates"))

    objects = WorkerFatalityManager()

    def clean(self):
        for attr in ("date_created", "date_modified"):
            if not getattr(self, attr):
                setattr(self, attr, now())
        for attr in ("date_death", "date_event"):
            if not getattr(self, attr):
                setattr(self, attr, None)

    def full_name(self):
        name = " ".join([n for n in (self.first_name, self.last_name) if n])
        if not name:
            name = _("Unknown name")
        return name

    def __str__(self):
        return "{}, {}".format(self.full_name(), self.date_death)

    class Meta:
        verbose_name = _("worker fatality")
        verbose_name_plural = _("worker fatalities")

    def to_dict(self):
        keys = ("id",)
        dct = {}
        for key in keys:
            dct[key] = getattr(self, key)
        dct['sources'] = list(self.sources.all())
        dct['contracts'] = list(self.contracts.all())
        if hasattr(self, "score"):
            dct['score'] = self.score
        return dct

    @property
    def admin_url(self):
        return reverse("admin:survey_workerfatality_change", args=(self.id,))

class UploadInProgress(models.Model):
    name = models.CharField(max_length=255)
    uploader = models.ForeignKey(User)
    pending_entries = JSONField()
    saved_entries = models.ManyToManyField(WorkerFatality, blank=True)
    complete = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def vivify_pending_entries(self):
        return [PendingEntry.from_dict(e) for e in self.pending_entries]

    def set_pending_entries(self, vivified):
        self.pending_entries = [e.to_dict() for e in vivified]

    def __str__(self):
        return "{} {}".format(self.name, self.created)

    def get_absolute_url(self):
        return reverse("edit_spreadsheet", args=[self.pk])

    class Meta:
        ordering = ['-created']
        verbose_name_plural = "Uploads in Progress"

class PendingEntry:
    """
    Stores the state of a potential new WorkerFataility entry prior to saving,
    so that we can search for duplicates, find errors, etc as part of batch
    updates. We don't just use model instances for this because we want to be
    able to store m2m's prior to saving.

    An alternative implementation -- which may or may not be more complex --
    would be to add a "pending" boolean to saved WorkerFatality entries and add
    a related model to define a "batch" of entries, along with
    creating/removing those batches as part of a workflow.

    Not persisted to the database; but serializable to JSON.
    """
    def __init__(self, row=None):
        self.m2m = defaultdict(list)
        self.fields = {}
        self.potential_duplicates = []
        self.row = row
        self.duplicate_action = {}

    def has_value(self, field, blanks=None):
        blanks = blanks or ["", None]
        desc = self.fields.get(field)
        return desc is not None and desc.get('value') not in blanks

    @property
    def errors(self):
        field_errs = {k: f for k,f in self.fields.items() if f['errors']}
        for k, desc_list in self.m2m.items():
            for desc in desc_list:
                if desc['errors']:
                    field_errs[k] = field_errs.get(k, [])
                    field_errs[k].append(desc)
        return field_errs

    @classmethod
    def from_dict(cls, dct):
        """
        Unserialize from a dictionary (e.g. loaded from json). Returns a
        PendingEntry instance.
        """
        pe = PendingEntry(dct['row'])

        # Vivify foreign keys
        for key, field_dict in dct.get('fields', {}).items():
            if isinstance(field_dict['value'], dict) and "id" in field_dict['value']:
                Field = WorkerFatality._meta.get_field(key)
                if not isinstance(Field, models.fields.related.ForeignKey):
                    raise Exception("Unrecognized foreign key {}".format(key))
                try:
                    field_dict['value'] = Field.related_model()._meta.model.objects.get(id=field_dict['value']['id'])
                except (KeyError, ObjectDoesNotExist):
                    if not field_dict.get('errors'):
                        field_dict['errors'] = ["Related field not found"]
            pe.fields[key] = field_dict

        # Vivify m2m's
        for key, field_dicts in dct.get('m2m', {}).items():
            Field = WorkerFatality._meta.get_field(key)
            if not isinstance(Field, models.fields.related.ManyToManyField):
                raise Exception("Unrecognized m2m key {}".format(key))
            Model = Field.related_model()._meta.model
            for field_dict in field_dicts:
                if isinstance(field_dict['value'], dict) and "id" in field_dict['value']:
                    field_dict['value'] = Model.objects.get(id=field_dict['value']['id'])
                pe.m2m[key].append(field_dict)

        # Vivify potential duplicates
        pe.duplicate_action = dct.get('duplicate_action', {})
        pks = [e['id'] for e in dct['potential_duplicates']]
        scores = {e['id']: e['score'] for e in dct['potential_duplicates']}
        if pks:
            pe.potential_duplicates = list(WorkerFatality.objects.filter(pk__in=pks))
            for wf in pe.potential_duplicates:
                setattr(wf, "score", scores[wf.id])
        else:
            pe.potential_duplicates = []
        return pe

    def replace_value(self, field_name, new_value, old_value=None):
        Field = WorkerFatality._meta.get_field(field_name)
        # Vivify relations
        if new_value is not None:
            if isinstance(Field, (models.fields.related.ForeignKey,
                                  models.fields.related.ManyToManyField)):
                Model = Field.related_model()._meta.model
                if isinstance(new_value, str):
                    try:
                        new_value = int(new_value)
                    except ValueError:
                        pass
                if isinstance(new_value, int):
                    new_value = Model.objects.get(id=new_value)
                elif not isinstance(new_value, Model):
                    raise Exception("Unknown type for relation to {}: '{}'".format(
                        Model._meta.verbose_name,
                        repr(new_value)))

        field_desc = None
        if field_name in self.m2m:
            for desc in self.m2m[field_name]:
                if desc['value'] == old_value:
                    field_desc = desc
                    break
        elif field_name in self.fields:
            field_desc = self.fields[field_name]
        if not field_desc:
            raise Exception("Unknown field {}".format(field_name))

        field_desc.update({'value': new_value, 'errors': []})

    def revalidate(self):
        """
        Append errors for any fields that do not currently have errors.
        """
        model = WorkerFatality(**self.clean_attrs())
        try:
            model.clean()
            model.full_clean()
        except ValidationError as e:
            for field, messages in e.message_dict.items():
                self.fields[field]['errors'] += messages
        if self.duplicate_action:
            self.potential_duplicates = []
        elif not self.errors:
            self.potential_duplicates = list(
                WorkerFatality.objects.find_potential_duplicates(
                    self, empty_penalty=0.8
                )[0:3]
            )

    def clean_attrs(self):
        attrs = {}
        for key, field_desc in self.fields.items():
            if field_desc['errors']:
                continue
            value = field_desc['value']
            # Swap None with emptystring if field doesn't support nulls.
            if value is None or value == "":
                Field = WorkerFatality._meta.get_field(key)
                if isinstance(Field, (models.CharField, models.TextField)):
                    attrs[key] = ""
                elif Field.null:
                    attrs[key] = None
                else:
                    pass # Skip the empty field.
            else:
                attrs[key] = value
        return attrs


    def save(self):
        model = WorkerFatality(**self.clean_attrs())
        model.clean()
        model.full_clean()

        if self.duplicate_action.get('overwrite'):
            model.pk = self.duplicate_action.get('overwrite')
            if WorkerFatality.objects.filter(pk=model.pk).exists():
                model.save(force_update=True)
            else:
                model.save(force_insert=True)
        else:
            model.save()
        for field_name, relations in self.m2m.items():
            setattr(model, field_name, [r['value'] for r in relations if r['value']])
        return model

    def to_dict(self):
        """
        Serialize to a dictionary (e.g. to put in json)
        """
        return {
            'row': self.row,
            'm2m': dict(self.m2m),
            'fields': self.fields,
            'potential_duplicates': [
                p.to_dict() for p in self.potential_duplicates
            ],
            'duplicate_action': self.duplicate_action
        }

class SiteCopy(models.Model):
    key = models.CharField(max_length=255, help_text="Machine name for this component")
    description = models.TextField()

    def __str__(self):
        return self.key
