from django.db import models
from django.db.models import Func, Expression, Case, When, Value

class FloatOutput(Func):
    def __init__(self, *args, **kwargs):
        super(FloatOutput, self).__init__(*args, output_field=models.FloatField(), **kwargs)

class Similarity(FloatOutput):
    function = 'similarity'
    
    def __init__(self, field_name, value):
        super(Similarity, self).__init__(models.F(field_name), models.Value(value))

class SimilarityDist(Similarity):
    function = 'similarity_dist'

class Product(FloatOutput):
    template = '(%(expressions)s)'
    arg_joiner = ' * '

class FloatSum(FloatOutput):
    template = '(%(expressions)s)'
    arg_joiner = ' + '

class ConditionalWeightedAverage(FloatOutput):
    """
    Given a list of (field_name, expression, weight) tuples, calculate the
    average of the value of 'expression' for the expressions applied to the
    field_name's, if the field name exists.
    """
    template = '(%(expressions)s)'
    arg_joiner = ' / '

    def __init__(self, field_expression_weights):
        super(ConditionalWeightedAverage, self).__init__(
            FloatSum(*[Case(When(**{field_name: "", "then": 0}),
                            When(**{field_name + "__isnull": True, "then": 0}),
                            default=Product(exp, weight)
                       ) for field_name,exp,weight in field_expression_weights]),
            FloatSum(*[Case(When(**{field_name: "", "then": 0}),
                            When(**{field_name + "__isnull": True, "then": 0}),
                            default=1
                       ) for field_name,exp,weight in field_expression_weights]),
        )

class Power(FloatOutput):
    template = '%(expressions)s'
    arg_joiner = '^'

class AbsDiff(FloatOutput):
    template = 'abs(%(expressions)s)'
    arg_joiner = ' - '

def ZeroIfEqual(field_name, val):
    if isinstance(val, models.Model):
        val = val.id
        field_name = field_name + "_id"
    return Case(When(**{field_name: Value(val), "then": 0}), default=1)

class DateDiff(FloatOutput):
    template = 'abs(%(expressions)s)'
    arg_joiner = ' - '

    def __init__(self, field, value):
        return super(DateDiff, self).__init__(
                field,
                Value(value, output_field=models.DateField()))
            
class ConditionalWeightedDistance(FloatOutput):
    template = 'sqrt(%(expressions)s)'
    arg_joiner = ' + '

    def __init__(self, field_expression_weights, penalty=0.5):
        cases = []
        for field_name, empties, exp, weight in field_expression_weights:
            whens = []
            for empty in empties:
                if empty is None:
                    whens.append(When(**{field_name + "__isnull": True, "then": penalty}))
                else:
                    whens.append(When(**{field_name: "", "then": penalty}))
            cases.append(
                Case(*whens, default=Power(Product(exp, weight), 2))
            )
        super(ConditionalWeightedDistance, self).__init__(*cases)

