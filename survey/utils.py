import os
import xlrd
import csv
from collections import namedtuple
from django.utils.translation import ugettext_lazy as _
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.contrib.auth.models import User

from survey import cell_filters as f
from survey.models import PendingEntry

class SheetException(Exception):
    pass

CellParser = namedtuple("CellParser", ["function", "field"])
IGNORE = CellParser(None, None)

class Format:
    ignored_columns = {
        "affil/union", "affil/temp", "affil/day", "affil/guest", "affil/public",
        "affil/contractor", "exposure/violence", "exposure/transportation",
        "exposure/fires", "exposure/falls", "exposure/exposure",
        "exposure/contact", "exposure/overexertion", "exposure/other",
        "exposure/unknown",
    }
    required_columns = {
        'today': CellParser(f.parse_date_to_datetime, "date_created"),
        'first_name': CellParser(f.filter_unknown, "first_name"),
        'last_name': CellParser(f.filter_unknown, "last_name"),
        "age": CellParser(f.int_or_none, "age"),
        "sex": CellParser(f.sex, "sex"),
        "date_event": CellParser(f.parse_date, "date_event"),
        "date_death": CellParser(f.parse_date, "date_death"),
        "event": CellParser(str, "event"),
        "event_city": CellParser(str, "event_city"),
        "event_state": CellParser(f.state, "event_state"),
        "occupation": CellParser(f.occupation, "occupation"),
        "industry": CellParser(f.naics, "industry"),
        "immigr": CellParser(f.yepnope, "immigrant"),
        "origin_country": CellParser(f.country, "origin_country"),
        "origin_city": CellParser(str, "origin_city"),
        "origin_state": CellParser(f.filter_unknown, "origin_state"),
        "direct_employer": CellParser(f.employer, "direct_employer"),
        "contract_1": CellParser(f.contract, f.m2m("contracts")),
        "contract_2": CellParser(f.contract, f.m2m("contracts")),
        "source_1": CellParser(f.source, f.m2m("sources")),
        "source_2": CellParser(f.source, f.m2m("sources")),
        "source_3": CellParser(f.source, f.m2m("sources")),
        "photo": CellParser(f.file_from_url, "photo"),
        "collector": CellParser(f.collector, "collector")
    }
    optional_columns = {
        "affil": CellParser(f.affiliation, "affiliation"),
        "exposure": CellParser(f.exposure, "exposure"),
    }
    alternate_names = {
        'origin_county': 'origin_country',
        'date_created': 'today',
        'immigrant': 'immigr',
        'affiliation': 'affil',
        'sources_1': 'source_1',
        'sources_2': 'source_2',
        'sources_3': 'source_3',
        'contracts_1': 'contract_1',
        'contracts_2': 'contract_2',
        'contracts_3': 'contract_3',
    }

    def ignore(self, key):
        return key in self.ignored_columns

    def parse(self, key, value):
        if key in self.required_columns:
            return self._parse(self.required_columns[key], value)
        elif key in self.optional_columns:
            return self._parse(self.optional_columns[key], value)
        elif key in self.alternate_names:
            return self.parse(self.alternate_names[key], value)
        else:
            return (None, None, None)

    def _parse(self, parser, value):
        try:
            parsed_value = parser.function(value)
            error = None
        except f.CellException as e:
            parsed_value = value
            error = str(e)
        return (parser.field, parsed_value, error)

    def match(self, heading):
        header_set = set(heading)
        required_set = set(self.required_columns.keys())
        extra = []
        for key in heading:
            if key in self.ignored_columns:
                continue
            elif key in self.alternate_names:
                header_set.remove(key)
                header_set.add(self.alternate_names[key])
                continue
            elif key in self.optional_columns or \
                 key in self.required_columns:
                continue
            else:
                extra.append(key)
        missing = required_set - header_set
        return extra, missing
import_formats = [Format()]

def read_spreadsheet(filename):
    """
    Given a spreadsheet filename, read the spreadsheet, detect its format,
    parse all the cells, and return an array of PendingEntry objects that
    indicate cell errors and potential duplicates.

    If the spreadsheet is not valid, throws a SheetException.
    """
    name, ext = os.path.splitext(filename)
    if ext.lower() in (".xls", ".xlsx"):
        rows = read_xlsx(filename)
    elif ext.lower() in (".csv",):
        exception = None
        for encoding in ('utf-8', 'latin1'):
            try:
                rows = read_csv(filename, encoding)
                break
            except UnicodeDecodeError as e:
                exception = e
                continue
        else:
            raise exception

    else:
        raise SheetException(_("Only xls, xlsx, and csv files are supported"))
    if len(rows) == 0:
        raise SheetException(_("Spreadsheet appears to be empty"))

    entries = parse_rows(rows)
    return entries

def read_xlsx(filename):
    """
    Given an .xls or .xlsx filename, return a 2d array of Cell objects.
    """
    try:
        book = xlrd.open_workbook(filename)
        sheet = book.sheet_by_index(0)
    except Exception as e:
        raise SheetException(e)
    header = [sheet.cell(0, i).value for i in range(sheet.ncols)]
    rows = []
    for i in range(1, sheet.nrows):
        row = []
        empty = True
        for j in range(sheet.ncols):
            cell = sheet.cell(i, j)
            row.append({
                "heading": header[j],
                "value": cell.value,
                "type": cell.ctype,
                "col": j,
                "row": i
            })
            if cell.value:
                empty = False
        if not empty:
            rows.append(row)
    return rows

def read_csv(filename, encoding='utf-8'):
    """
    Given a .csv filename, return a 2d array of Cell objects.
    """
    with open(filename, encoding=encoding) as fh:
        reader = csv.reader(fh)
        header = None
        rows = []
        for i, row in enumerate(reader):
            if i == 0:
                header = row
                continue
            cells = []
            empty = True
            for j in range(len(row)):
                cells.append({
                    "heading": header[j],
                    "value": row[j],
                    "type": None,
                    "col": j,
                    "row": i
                })
                if row[j]:
                    empty = False
            if not empty:
                rows.append(cells)
        return rows

def parse_rows(rows):
    """
    Parse the rows from a spreadsheet.
    """
    headings = [cell['heading'] for cell in rows[0]]
    column_error = None
    for import_format in import_formats:
        extra, missing = import_format.match(headings)
        if missing:
            errors = []
            if missing:
                errors.append("missing columns: {}".format(missing))
            if extra:
                errors.append("extra columns: {}".format(extra))
            column_error = "\n".join(errors)
        else:
            break
    else:
        raise SheetException("Unrecognized column format: {}".format(column_error))

    entries = []

    for row in rows:
        entry = PendingEntry(row[0]['row'])
        for cell in row:
            errors = []
            pfield, value, err = import_format.parse(cell['heading'], cell['value'])
            if pfield is None:
                continue
            if err:
                errors.append(err)

            field_desc = {
                "value": value,
                "cell": cell,
                "errors": errors
            }
            if isinstance(pfield, f.m2m):
                if value is not None:
                    entry.m2m[str(pfield)].append(field_desc)
            else:
                entry.fields[str(pfield)] = field_desc
        if not entry.errors:
            entry.revalidate()
        entries.append(entry)
    return entries

class PendingEntryJSONEncoder(DjangoJSONEncoder):
    def default(self, o):
        t = type(o)
        # Resolve ugettext_lazy stuff.  This may not be correct for other lazy
        # values that do not resolve to strings.
        if t.__module__ == "django.utils.functional" and t.__name__ == "__proxy__":
            return str(o)
        # Do light-weight model serialization.
        if isinstance(o, models.Model):
            if hasattr(o, "to_dict"):
                dct = o.to_dict()
                dct.update({"str": str(o)})
                return dct
            if isinstance(o, User):
                return {"id": o.id, "username": o.username, "str": str(o)}
            return {"id": o.id, "str": str(o)}
        return super(PendingEntryJSONEncoder, self).default(o)

