function initDatepickers() {
  ["id_date_event", "id_date_death"].forEach(function(id) {
    let pd = new Pikaday({
      field: document.getElementById(id),
      format: 'YYYY-MM-DD',
      // TODO: js-i18n
      i18n: {
        previousMonth: gettext('Previous Month'),
        nextMonth: gettext('Next Month'),
        months: [
          gettext('January'),
          gettext('February'),
          gettext('March'),
          gettext('April'),
          gettext( 'May'),
          gettext( 'June'),
          gettext( 'July'),
          gettext( 'August'),
          gettext( 'September'),
          gettext( 'October'),
          gettext( 'November'),
          gettext( 'December')
        ],
        weekdays      : [
          gettext('Sunday'),
          gettext( 'Monday'),
          gettext( 'Tuesday'),
          gettext( 'Wednesday'),
          gettext( 'Thursday'),
          gettext( 'Friday'),
          gettext( 'Saturday')
        ],
        weekdaysShort : [
          gettext('Sun'),
          gettext( 'Mon'),
          gettext( 'Tue'),
          gettext( 'Wed'),
          gettext( 'Thu'),
          gettext( 'Fri'),
          gettext('Sat')]
      }
    });
  });
}
function initSelect2s() {
  $("#id_occupation").selectize();
  $("#id_event_state").selectize();
  $("#id_industry").selectize();
  $("#id_origin_country").selectize();
  $("#id_sources, #id_contracts").selectize({
    create: function(input) {
      return {
        value: "new-" + input,
        text: input,
      };
    },
    delimiter: ',',
    createOnBlur: true,
    openOnFocus: false,
    maxItems: 100,
    persist: false,
    hideSelected: true,
    closeAfterSelect: true,
    render: {
      option_create: function(data, escape) {
        return `<div class="create">${gettext("Add")} <strong>${escape(data.input)}</strong>&hellip;</div>`;
      }
    },
    plugins: ['restore_on_backspace', 'remove_button']
  });
}

$(document).ready(function() {
  initDatepickers();
  initSelect2s();
});
