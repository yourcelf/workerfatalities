const PendingSpreadsheetForm = React.createClass({
  unpackData: function (data) {
    // To keep things snappy, we keep the JSON structures for errors and
    // duplicates a bit DRY: keys and values are separated into arrays to avoid
    // repeating the keys for each row, and especially, choice lists for
    // constrained choice fields are split out.  This reduces the data size
    // by around 3-4 orders of magnitude, which is quite noticable.
    //
    // For convenience, unpack them here.
    let entries = [];
    data.errors.data.forEach((array) => {
      let entry = {};
      for (let i = 0; i < data.errors.heading.length; i++) {
        entry[data.errors.heading[i]] = array[i];
      }
      let fieldInfo = data.errors.fields[entry.field_name];
      entry.verbose_name = fieldInfo.verbose_name;
      entry.constraints = fieldInfo.constraints; 
      entry.key = [entry.field_name, entry.row].join("-");
      entries.push(entry);
    });

    // Sort rows by row number numerically
    let dupRows = Object.keys(
      data.duplicates.rows
    ).sort((a, b) => parseInt(a, 10) < parseInt(b, 10) ? -1 : b === a ? 0 : 1);
    // Assemble potential duplicates.
    let duplicates = {};
    let vivifyDuplicate = function(array) {
      let obj = {};
      for (let i = 0; i < data.duplicates.heading.length; i++) {
        obj[data.duplicates.heading[i]] = array[i];
      }
      obj._meta = data.duplicates.fields;
      return obj;
    };
    dupRows.forEach((row) => {
      duplicates[row] = {
        'entry': vivifyDuplicate(data.duplicates.rows[row].entry),
        'duplicates': data.duplicates.rows[row].duplicates.map(vivifyDuplicate)
      }
    });

    return {
      "entries": entries,
      "duplicates": duplicates,
    };
  },
  getInitialState: function() {
    let state = this.unpackData(this.props.data);
    return state;
  },
  updateState: function(params, cb) {
    if (params && params.action === "merge") {
      document.location.href = this.props.updateEntryUrl +
        `?row=${params.params.row}&pk1=${params.params.duplicate.id}`;
    } else {
      jQuery.ajax({
        type: "post",
        url: this.props.updateEntryUrl,
        data: params,
        success: (data) => {
          console.log("success", data);
          if (data.errors.data.length === 0 && Object.keys(data.duplicates.rows).length === 0) {
            ReactDOM.findDOMNode(this.refs.submit).click()
          } else {
            let state = this.unpackData(data);
            this.setState(state);
            cb && cb();
          }
        },
        error: () => {
          alert("Error updating");
          console.log(arguments);
          cb && cb();
        }
      });
    }
  },
  renderMerge: function() {
    let {entry, duplicate, row} = this.state.merge;
    let wf1 = Object.assign({}, duplicate);
    wf1.repr = `record ${duplicate.id}`;
    let wf2 = Object.assign({}, entry);
    wf2.repr = `row ${row}`;

    return <MergeRecordsForm
        wf1={wf1}
        wf2={wf2}
        onDone={() => Promise.resolve().then(() => this.updateState({
            action: "merge", params: null
        }))}
        onDeleteWf2={() => Promise.resolve().then(() => this.updateState({
          action: "discard_row", row: row
        }))}
        onKeepBoth={() => Promise.resolve().then(() => this.updateState({
          action: "nonduplicates", id: duplicate.id
        }))}
      />
  },
  render: function() {
    if (this.state.merge) {
      return this.renderMerge()
    }
    // Sort entries by row.
    let errorRows = [];
    let errorRow;
    let curRowIndex = null;
    this.state.entries.forEach(function(entry) {
      if (curRowIndex !== entry.row) {
        curRowIndex = entry.row;
        errorRow = {row: entry.row, entries: []}
        errorRows.push(errorRow);
      }
      errorRow.entries.push(entry);
    });
    let renderedErrorRows = errorRows.length > 20 ? errorRows.slice(0, 20) : errorRows;
    let flatDupes = _.map(this.state.duplicates, (obj, row) => [obj, row]);
    let renderedDupes = flatDupes.length > 20 ? flatDupes.slice(0, 20) : flatDupes;

    return (
      <div>
        <ul className='messagelist'>
          <li className='warning'>
            Please correct these spreadsheet problems before continuing. {errorRows.length} errors, {flatDupes.length} potential duplicates. 
          </li>
        </ul>
        { errorRows.length === 0 ? "" : 
          <div className='rows'>
            <h2>Errors ({errorRows.length})</h2>
            {renderedErrorRows.map((row) => (
              <div key={`row-${row.row}`} className='row-errors'>
                <h3>{interpolate(gettext('Row %s'), [row.row + 1])}</h3>
                {row.entries.map((entry) => <EntryError
                                              entry={entry}
                                              key={entry.key}
                                              updateState={this.updateState} />)}
              </div>
            ))}
            {renderedErrorRows.length < errorRows.length ?
              <div><b>{ errorRows.length - renderedErrorRows.length } additional errors not shown yet.</b></div>
              : <div></div>
            }
          </div>
        }
        { flatDupes.length === 0 ? "" :
          <div className='rows'>
            <h2>Duplicate Check ({flatDupes.length})</h2>
            <p>
              <span>Most similar &rarr;</span>{' '}
              <span className='harvey-ball' title='similarity: 100%'>
                <img src='/static/survey/img/harvey100.svg' width='16' height='16' />
              </span>{' '}
              <span className='harvey-ball' title='similarity: 80%'>
                <img src='/static/survey/img/harvey80.svg' width='16' height='16' />
              </span>{' '}
              <span className='harvey-ball' title='similarity: 60%'>
                <img src='/static/survey/img/harvey60.svg' width='16' height='16' />
              </span>{' '}
              <span className='harvey-ball' title='similarity: 40%'>
                <img src='/static/survey/img/harvey40.svg' width='16' height='16' />
              </span>{' '}
              <span className='harvey-ball' title='similarity: 20%'>
                <img src='/static/survey/img/harvey20.svg' width='16' height='16' />
              </span>{' '}
              <span>&larr; least similar</span>
            </p>
            <div className='table'>
              <div className='table-header-group'>
                <div className='table-row'>
                    <div className='table-cell head'></div>
                    <div className='table-cell head'>Row</div>
                    <div className='table-cell head'></div>
                    <div className='table-cell head'>Name</div>
                    <div className='table-cell head'>City</div>
                    <div className='table-cell head'>State</div>
                    <div className='table-cell head'>Date</div>
                    <div className='table-cell head'>Description</div>
                </div>
              </div>
              {_.map(renderedDupes, ([obj, row]) => {
                return _.map(obj.duplicates, (duplicate, i) => {
                  return <DuplicatePair
                            entry={obj.entry}
                            duplicate={duplicate}
                            row={row}
                            key={`row-${row}-${i}`}
                            updateState={this.updateState} />;
                });
              })}
            </div>
           {renderedDupes.length < flatDupes.length ?
              <div><b>{ flatDupes.length - renderedDupes.length } additional potential duplicates not shown yet.</b></div>
              : <div></div>
            } 
          </div>
        }
        <button type='submit' ref='submit' style={{display: "none"}}>Submit</button>
      </div>
    )
  }
});

const repr = function (val) {
  if (!val) { return "-"; }
  if (val.str) { return val.str; }
  if (Array.isArray(val)) { return val.map(repr).join(", ") || "-"; }
  return val;
}

const ReplaceWidget = React.createClass({
  render: function() {
    let props = Object.assign({
      ref: "replace"
    }, this.props)
    switch(this.props.constraints.type) {
      case "string":
        if (this.props.constraints.choices) {
          return <ChoiceInput {...props} />;
        } else if (this.props.constraints.max_length) {
          return <CharInput {...props} max={this.props.constraints.max_length} />;
        } else {
          return <TextInput {...props} />;
        }
      case "email":
        return <CharInput {...props} type="email" />
      case "int":
        return <CharInput {...props} type='text' pattern='\d*' />
      case "date":
        return <DateInput {...props} />
      case "related":
        this.props.constraints.choices.mapInitialValue = (v) => v && v.id;
        return <ChoiceInput {...props} />
      case "m2m":
        this.props.constraints.choices.mapInitialValue = (val) => val && val.map((v) => v.id);
        return <ChoiceInput {...props} multiple={true} />
      case "boolean":
        let constraints = Object.assign({}, props.constraints);
        constraints.choices = [
          ["1", "-----"],
          ["2", "Yes"],
          ["3", "No"],
        ];
        constraints.choices.mapInitialValue = (v) => {
          return {true: "2", false: "3"}[v] || "1";
        }
        return <ChoiceInput {...props} constraints={constraints} />
      case "readonly":
        return <span {...props}>{repr(props.value)}</span>
      default:
        alert("Unknown type " + this.props.constraints.type);
    }
  }
})

const EntryError = React.createClass({
  getInitialState: function() {
    return {};
  },
  doAction: function(event, action) {
    event.preventDefault();
    let entry = this.props.entry;
    let params = {
      field: entry.field_name,
      value: entry.value,
      row: entry.row,
      col: entry.col,
      replacement: this.refs.replace && this.refs.replace.refs.replace.state.value,
      action: action
    };
    this.setState({loading: true});
    this.props.updateState(params, () => {
      if (!this.unmounting) {
        this.setState({loading: false});
      }
    });
  },
  componentWillUnmount: function() {
    this.unmounting = true;
  },
  renderAction: function(action, text) {
    let button = <button type='button'
                         className='button'
                         onClick={(e) => this.doAction(e, action)}
                         disabled={!!this.state.loading}>{text}</button>;
    if (action === "replace") {
      return (
        <div>
          <ReplaceWidget
              id={`id-${this.props.entry.key}`}
              name={`name-${this.props.entry.key}`}
              key="replace"
              ref="replace"
              value={this.props.entry.value}
              constraints={this.props.entry.constraints} />
          {button}
        </div>
      )
    }
    return button;
  },
  render: function() {
    let entry = this.props.entry;
    let actionChoices = [];
    ["ignore", "add", "replace"].forEach((order) => {
      let choice = _.find(entry.action_choices, (c) => c[0] === order);
      if (choice) {
        actionChoices.push(choice);
      }
    });
    return <div className='cell-error'>
      <label htmlFor={`id-${entry.key}`}>
        { entry.error ?
          <span>
            Column {entry.col}, {entry.verbose_name}: "<span className='entry-value'>{entry.value}</span>" - {entry.error}.
          </span>
          : 
          <span>
            "<span className='entry-value'>{entry.value}</span>" is not a valid choice for {entry.verbose_name}.
          </span>
        }
        { this.state.loading ?
          <img src='/static/img/spinner.gif' alt={gettext('loading...')} /> : "" }
      </label>
      <ul>
        {actionChoices.map(([action, actionText], i) => {
          return <li key={`action-${entry.key}-${action}`}>
            {this.renderAction(action, actionText)}
          </li>
        })}
      </ul>
    </div>
  }
});
const CharInput = React.createClass({
  getInitialState: function() {
    return {value: this.props.value || ""}
  },
  onChange: function(event) {
    this.setState({value: event.target.value});
  },
  render: function() {
    return <input type={this.props.type || 'text'} value={this.state.value} onChange={this.onChange} pattern={this.props.pattern} />
  }
});
const TextInput = React.createClass({
  getInitialState: function() {
    return {value: this.props.value || ""}
  },
  onChange: function(event) {
    this.setState({value: event.target.value});
  },
  render: function() {
    return <textarea onChange={this.onChange} value={this.state.value} style={{width: "100%", height: "100%"}}></textarea>
  }
});
const DateInput = React.createClass({
  _buildPicker: function(el) {
    this.pd = new Pikaday({
      field: el,
      format: 'YYYY-MM-DD',
      onClose: () => {
        this.handleOnChange({target: el})
      }
    });
  },
  getInitialState: function() {
    return {value: this.props.value || ""};
  },
  componentDidMount: function() {
    this._buildPicker(ReactDOM.findDOMNode(this));
  },
  handleOnChange: function(event) {
    this.setState({value: event.target.value});
    if (this.props.onChange) {
      this.props.onChange(event);
    }
  },
  render: function() {
    return <input
      type='text'
      value={this.state.value}
      onChange={this.handleOnChange} />
  }
});
const ChoiceInput = React.createClass({
  getInitialState: function() {
    //return {value: this.props.value || undefined};
    return {};
  },
  onChange: function(event) {
    let value = event.target.value;
    this.setState({value: value});
    if (this.props.onChange) {
      this.props.onChange(event);
    }
  },
  componentWillUpdate: function() {
    if (this.$selectize) {
      this.$selectize[0].selectize.destroy()
    }
  },
  render: function() {
    //let mapper = this.props.constraints.choices.mapInitialValue || ((v) => v)
    //let value = mapper(this.state.value);
    return <select {...this.props}
      multiple={!!this.props.multiple}
      style={{maxWidth: "300px"}}
      value={this.state.value || ""}
      onChange={this.onChange}
      ref={(select) => {
        if (this.props.constraints.choices.length > 5) {
          let that = this;
          this.$selectize = jQuery(select).selectize({
            onChange: function (value) {
              setTimeout(function() {
                that.onChange({target: {value: value}});
              }, 0);
            }
          });
        }
      }}>
        {this.props.constraints.choices.map((choice, i) => {
          return <option key={`${this.props.id}-${i}`}
            value={choice[0]}>{choice[1]}</option>
        })}
    </select>
  }
});

const DuplicatePair = React.createClass({
  getInitialState: function() {
    return {};
  },
  onChange: function(event) {
    let value = event.target.value;
    this.setState({value: value});
  },
  updateState: function(props) {
    if (props.action !== "merge") {
      this.setState({loading: true});
    }
    this.props.updateState(_.extend({row: this.props.row}, props));
  },
  discardSpreadsheetEntry: function(event) {
    event.preventDefault();
    this.updateState({action: "discard_row"});
  },
  overwriteDatabaseEntry: function(event) {
    event.preventDefault();
    this.updateState({
      action: "overwrite_db_entry",
      id: this.props.duplicate.id
    });
  },
  mergeRecords: function(event) {
    event.preventDefault();
    this.updateState({
      action: "merge",
      params: {
        entry: this.props.entry,
        duplicate: this.props.duplicate,
        row: this.props.row
      }
    });
  },
  setNotDuplicate: function(event) {
    event && event.preventDefault();
    this.updateState({
      action: "nonduplicates",
      id: this.props.duplicate.id
    });
  },
  renderRow: function(heading, action, entry, key) {
    return (
      <div className='table-row' key={key}>
        <div className='table-cell'>{action}</div>
        <div className='table-cell head'>{heading}</div>
        <div className='table-cell' title={entry.score}>
          {_.isNumber(entry.score) ?
            entry.score < 0.4 ?
              <img src='/static/survey/img/harvey100.svg' width='16' height='16' /> :
            entry.score < 0.8 ?
              <img src='/static/survey/img/harvey80.svg' width='16' height='16' /> :
            entry.score < 1.2 ?
              <img src='/static/survey/img/harvey60.svg' width='16' height='16' /> :
            entry.score < 1.6 ?
              <img src='/static/survey/img/harvey40.svg' width='16' height='16' /> :
            entry.score <= 2.0 ?
              <img src='/static/survey/img/harvey20.svg' width='16' height='16' /> : ""
           : ""}
        </div>
        <div className='table-cell'>
          { entry.first_name || entry.last_name ? 
             <span>{entry.first_name} {entry.last_name}</span>
             : <span>&ndash;</span>}
        </div>
        <div className='table-cell'>{entry.event_city}</div>
        <div className='table-cell'>{entry.event_state}</div>
        <div className='table-cell'>{entry.date_death || entry.date_event}</div>
        <div className='table-cell'>
          <em title={entry.event}>
            {entry.event ? entry.event.substring(0, 50) + "...": ""}
          </em>
        </div>
      </div>
    )
  },
  render: function() {
    return <div className='table-row-group'>
      {this.renderRow(
        (interpolate(gettext('Row %s:'), [this.props.row])),
        (<button className='button hundred' onClick={this.discardSpreadsheetEntry}
            disabled={!!this.state.loading}>Discard row</button>),
        this.props.entry)}
      {this.renderRow(
        (
          <a href={this.props.duplicate.admin_url} target='_blank'>
            db {this.props.duplicate.id}
           </a>
        ),
        (<button className='button hundred' onClick={this.mergeRecords}
            disabled={!!this.state.loading}>Merge</button>),
        this.props.duplicate)}
      <div className='table-row'>
        <div className='table-cell'>
          <button className='button hundred' onClick={this.setNotDuplicate}
            disabled={!!this.state.loading}>Keep Both</button>
        </div>
        <div className='table-cell'>
          {this.state.loading ? <img src='/static/img/spinner.gif' alt='Loading...' /> : ""}
        </div>
      </div>
      <div className='table-row head'>
        <div className='table-cell'>&nbsp;</div>
        <div className='table-cell'></div>
        <div className='table-cell'></div>
        <div className='table-cell'></div>
        <div className='table-cell'></div>
        <div className='table-cell'></div>
        <div className='table-cell'></div>
        <div className='table-cell'></div>
      </div>
    </div>
  }
});

//const MergeRecordsForm = React.createClass({
//  fields: [
//      "first_name", "last_name", "age", "sex", "date_event", "date_death",
//      "event", "event_city", "event_state", "occupation", "industry",
//      "immigrant", "affiliation", "exposure", "origin_country", "origin_city",
//      "origin_state", "direct_employer", "notes", "email"
//  ],
//
//  isFieldIdenticalOrEmpty: function(field_name) {
//    if (this.props.wf2[field_name] === this.props.wf1[field_name]
//        && this.props.wf2[field_name]) {
//      return true;
//    }
//    /*
//    if (this.props.wf2[field_name] === "" || this.props.wf2[field_name] === null) {
//      return true;
//    }
//    */
//    return false;
//  },
//  getInitialState: function() {
//    let state = {fields: {}, loading: false};
//    this.fields.forEach((field) => {
//      state.fields[field] = this.props.wf1[field];
//    });
//    return state;
//  },
//  stateHandler: function(key) {
//    return (event) => {
//      let fields = Object.assign({}, this.state.fields);
//      fields[key] = event.target.value;
//      this.setState({fields: fields})
//    }
//  },
//  handleError: function(err) {
//    console.log(err);
//    this.setState({loading: false})
//    alert("Server error");
//  },
//  onMerge: function(event) {
//    event && event.preventDefault();
//    this.setState({"loading": true});
//    jQuery.ajax({
//      method: "POST",
//      url: `/admin/survey/workerfatality/${this.props.wf1.id}/`,
//      data: this.state.fields,
//      success: () => this.onDeleteWf2(),
//      error: (err) => this.handleError(err),
//    });
//  },
//  onKeepBoth: function(event) {
//    event && event.preventDefault();
//    this.setState({"loading": true});
//    this.onKeepBoth()
//    .then(this.onDone)
//    .catch((err) => this.handleError(err));
//  },
//  onDeleteWf2: function(event) {
//    event && event.preventDefault();
//    this.setState({"loading": true});
//    this.props.onDeleteWf2()
//    .then(this.onDone)
//    .catch((err) => this.handleError(err));
//  },
//  onDeleteWf1: function(event) {
//    event && event.preventDefault();
//    if (confirm(`Are you sure you want to delete ${this.props.wf1.repr}?`)) {
//      this.setState({loading: true});
//      jQuery.ajax({
//        method: "POST",
//        url: `/admin/survey/workerfatality/${this.props.wf1.id}/delete/`,
//        success: () => this.onDone,
//        error: (err) => this.handleError(err)
//      });
//    }
//  },
//  onDone: function(event) {
//    event && event.preventDefault();
//    this.setState({loading: true});
//    this.props.onDone();
//  },
//  render: function() {
//    return (
//      <div className='merge-records'>
//        <h1>Merge Worker Fatality Records</h1>
//        <form className='merge-records' onSubmit={this.onMerge}>
//          <table>
//            <thead>
//              <tr>
//                <th></th>
//                <th>
//                  { this.props.compType === "db"
//                    ? <a href={this.props.wf2.admin_url} target='_blank'>
//                        {this.props.wf2.repr}
//                      </a>
//                    : <span>{this.props.wf2.repr}</span>
//                  }
//                </th>
//                <th>
//                  <a href={this.props.wf1.admin_url} target='_blank'>
//                    {this.props.wf1.repr}
//                  </a>
//                </th>
//              </tr>
//            </thead>
//            <tbody>
//            {
//              this.fields.map((field, i) => {
//                return (
//                  <tr key={i}>
//                    <th><nobr>{this.props.wf1._meta[field].verbose_name}</nobr></th>
//                    <td className='entry1'>{repr(this.props.wf2[field])}</td>
//                    <td className='entry2'>
//                      { this.isFieldIdenticalOrEmpty(field)
//                        ? <span>{repr(this.props.wf1[field])}</span>
//                        : <ReplaceWidget
//                            value={this.state.fields[field]}
//                            constraints={this.props.wf1._meta[field].constraints}
//                            onChange={this.stateHandler(field)} />
//                      }
//                    </td>
//                  </tr>
//                )
//              })
//            }
//            </tbody>
//            <tbody>
//              <tr>
//                <th></th>
//                <td>
//                  {this.state.loading ? "" : 
//                    <a href='#' className='deletelink' onClick={this.onDeleteWf2}>
//                      Delete {this.props.wf2.repr}
//                    </a>
//                  }
//                </td>
//                <td>
//                  {this.state.loading ? "" : 
//                    <a href='#' className='deletelink' onClick={this.onDeleteWf1}>
//                      Delete {this.props.wf1.repr}
//                    </a>
//                  }
//                </td>
//              </tr>
//              <tr>
//                <td>
//                  { this.state.loading ? "" : 
//                    <a href='#' className='cancel' onClick={this.onDone}>
//                      &larr; Cancel
//                    </a>
//                  }
//                </td>
//                <td>
//                  <button className='button' onClick={this.onKeepBoth} disabled={this.state.loading}>
//                    Not Duplicates, keep both
//                  </button>
//                </td>
//                <td>
//                  <button className='button' disabled={this.state.loading}>
//                    Save merged {this.props.wf1.repr}
//                  </button>
//                  {' '}
//                  <span>{this.props.wf2.repr} will be deleted.</span>
//                </td>
//              </tr>
//            </tbody>
//          </table>
//        </form>
//      </div>
//    );
//  }
//});
