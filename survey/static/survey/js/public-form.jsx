function initDatepickers() {
  ["id_date_event", "id_date_death"].forEach(function(id) {
    let pd = new Pikaday({
      field: document.getElementById(id),
      format: 'YYYY-MM-DD',
      i18n: {
        previousMonth: gettext('Previous Month'),
        nextMonth: gettext('Next Month'),
        months: [
          gettext('January'),
          gettext('February'),
          gettext('March'),
          gettext('April'),
          gettext( 'May'),
          gettext( 'June'),
          gettext( 'July'),
          gettext( 'August'),
          gettext( 'September'),
          gettext( 'October'),
          gettext( 'November'),
          gettext( 'December')
        ],
        weekdays      : [
          gettext('Sunday'),
          gettext( 'Monday'),
          gettext( 'Tuesday'),
          gettext( 'Wednesday'),
          gettext( 'Thursday'),
          gettext( 'Friday'),
          gettext( 'Saturday')
        ],
        weekdaysShort : [
          gettext('Sun'),
          gettext( 'Mon'),
          gettext( 'Tue'),
          gettext( 'Wed'),
          gettext( 'Thu'),
          gettext( 'Fri'),
          gettext('Sat')]
      }
    });
  });
}

function initSelect2s() {
  $("#id_occupation").selectize();
  $("#id_event_state").selectize();
  $("#id_industry").selectize();
  $("#id_origin_country").selectize();
  $("#id_sources, #id_contracts").selectize({
    create: function(input) {
      return {
        value: "new-" + input,
        text: input,
      };
    },
    delimiter: ',',
    createOnBlur: true,
    openOnFocus: false,
    maxItems: 100,
    persist: false,
    hideSelected: true,
    closeAfterSelect: true,
    render: {
      option_create: function(data, escape) {
        return `<div class="create">${gettext("Add")} <strong>${escape(data.input)}</strong>&hellip;</div>`;
      }
    },
    plugins: ['restore_on_backspace', 'remove_button']
  });
}

const DuplicateList = React.createClass({
  getInitialState: function() {
    return {
      loading: false,
      error: false,
      duplicates: []
    }
  },
  render: function() {
    let getBasicParts = function(dup) {
      return [
        ['event_city', <span className='event-city'>{dup.event_city}{dup.event_state ? ", " : ""}</span>],
        ['event_state', <span className='event-state'>{dup.event_state}</span>],
        ['sex', <span className='sex'>{dup.sex}</span>],
        ['age', <span className='age'>{dup.age} years old</span>],
        ['immigrant', <span className='immigrant'>{dup.immigrant ? "immigrant" : ""}</span>],
        ['origin_country', <span className='origin-country'>{dup.origin_country && dup.origin_country.name}</span>],
      ].map(([key, el]) => dup[key] ? el : "")
    }

    let getDateFormat = function(dup) {
      let date = dup.date_death || dup.date_event;
      return date ? moment(date).format("YYYY-MM-DD") : "";
    }

    return <div className='potential-duplicates-holder'>
      <div className='potential-duplicates'>
        {this.state.loading ?  <img className='loading' src='/static/img/spinner.gif' /> : ""}
        {this.state.error ? <div className='alert alert-danger'>
             Server error when looking for duplicates.
           </div> : ""}
        {this.state.duplicates.length > 0 ?
          <div className='alert alert-info'>{window.DUPLICATES_EXPLANATION}</div>
         : ""}

        {this.state.duplicates.map((dup) => {
          return <div className='potential-duplicate' key={`dup-${dup.id}`} data-score={dup.score}>
            <div className='basic'>
              <span className='name'>
                {dup.first_name || dup.last_name ?
                  <span>{dup.first_name} {dup.last_name}</span>
                 : <span>{gettext('Unknown name')}</span>
                }{' '}
              </span>
              {getBasicParts(dup).map((el) => [el, <span> </span>])}
            </div>
            <div className='description'>
              <span className='date'>{getDateFormat(dup)}</span> {dup.event} <span className='score'>{dup.score.toFixed(2)}</span>
            </div>
          </div>
        })}
      </div>
    </div>
  }
});

function attachDuplicateChecks() {
  let duplicateList = ReactDOM.render(<DuplicateList />,
                                      document.getElementById('duplicates'));
  let dupFields = [
    "#id_first_name",
    "#id_last_name",
    "#id_event_city",
    "#id_event_state",
    "#id_sex",
    "#id_age",
    "#id_immigrant",
    "#id_origin_city",
    "#id_origin_country",
    "#id_date_event",
    "#id_date_death"
  ];
  let fetchPotentialDuplicates = _.debounce(function() {
    let data = {};
    dupFields.forEach(function(field) {
      let el = $(field);
      data[el.attr("name")] = el.val();
    });
    $.ajax({
      url: "/duplicates-ajax/",
      type: "POST",
      data: data,
      success: function(data) {
        duplicateList.setState({
          loading: false,
          error: false,
          duplicates: data.duplicates
        });
      },
      error: function() {
        console.error(arguments);
        duplicateList.setState({loading: false, error: true});
      }
    });
  }, 200);

  $(dupFields.join(",")).on("change", function(event) {
    duplicateList.setState({loading: true, error: false});
    fetchPotentialDuplicates();
  });
  $(dupFields.join(",")).on("keyup", function(event) {
    duplicateList.setState({loading: true, error: false});
    fetchPotentialDuplicates();
  });
  fetchPotentialDuplicates();
}

$(document).ready(function() {
  initDatepickers();
  initSelect2s();
  attachDuplicateChecks();
});
