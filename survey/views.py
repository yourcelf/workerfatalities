import csv
import json
import datetime
from io import StringIO
from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import user_passes_test
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.http import HttpResponseBadRequest, HttpResponse, Http404, JsonResponse
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt
from survey import forms
from survey import utils
from survey.models import WorkerFatality, Collector, UploadInProgress, PendingEntry

def staff_check(user):
    return user.is_staff

@user_passes_test(staff_check)
def spreadsheet(request):
    form = forms.UploadForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        pending_entries = form.cleaned_data['pending_entries']
        edit_form = forms.EntryConfirmationForm(entries=pending_entries)

        upload = UploadInProgress(
            name=request.FILES['file'].name,
            uploader=request.user
        )
        upload.pending_entries = json.loads(edit_form.data['pending_entries_json'])
        upload.save()

        if edit_form.is_valid():
            upload.complete = True
            upload.saved_entries = edit_form.save()
            upload.save()
            return redirect("spreadsheet_saved")
        else:
            return redirect("edit_spreadsheet", pk=upload.pk)
    
    context = {'form': form}
    context.update(admin.site.each_context(request))
    return render(request, "admin/spreadsheet.html", context)

@user_passes_test(staff_check)
def edit_spreadsheet(request, pk):
    try:
        upload = UploadInProgress.objects.get(pk=pk)
    except UploadInProgress.DoesNotExist:
        raise Http404

    if upload.complete:
        return redirect("spreadsheet_saved", pk=upload.id)

    form = forms.EntryConfirmationForm(data={
        "pending_entries_json": json.dumps(upload.pending_entries)
    })
    valid = form.is_valid()
    if valid:
        upload.saved_entries = form.save()
        upload.complete = True
        upload.save()
        return redirect("spreadsheet_saved", pk=upload.id)

    context = {
        'upload': upload,
        'form': form,
        'update_entry_url': reverse("update_pending_spreadsheet_entry", args=[upload.id])
    }
    context.update(admin.site.each_context(request))
    return render(request, "admin/spreadsheet-edit.html", context)

@user_passes_test(staff_check)
def update_entry(request, pk):
    try:
        upload = UploadInProgress.objects.get(pk=pk)
    except UploadInProgress.DoesNotExist:
        raise Http404
    if request.method == 'GET' and request.GET.get("row") and request.GET.get("pk1"):
        return redirect(
            reverse("merge_worker_fatalities") + "?row={}&upload={}&pk1={}".format(
                request.GET.get("row"), pk, request.GET.get("pk1")
            )
        )

    if request.method != "POST":
        return HttpResponseBadRequest("POST only")

    form = forms.EntryConfirmationForm(data={
        "pending_entries_json": json.dumps(upload.pending_entries)
    })
    try:
        form.update_entry(request.POST)
    except ValidationError as e:
        return HttpResponseBadRequest(str(e))

    upload.pending_entries = json.loads(form.data['pending_entries_json'])
    upload.save()
    if request.is_ajax():
        response = HttpResponse()
        response['Content-type'] = "application/json"
        response.write(form.data_json())
        return response
    return redirect(upload.get_absolute_url())

@user_passes_test(staff_check)
def spreadsheet_saved(request, pk):
    try:
        upload = UploadInProgress.objects.get(pk=pk)
    except UploadInProgress.DoesNotExist:
        raise Http404

    context = {"upload": upload}
    context.update(admin.site.each_context(request))
    return render(request, "admin/spreadsheet-saved.html", context)

@user_passes_test(staff_check)
def delete_then_check_saved_duplicates(request):
    if request.method == "POST":
        try:
            wf = WorkerFatality.objects.get(pk=int(request.POST.get("pk")))
        except (ValueError, WorkerFatality.DoesNotExist):
            pass
        else:
            wf.delete()
    return redirect("check_saved_duplicates")

@user_passes_test(staff_check)
def check_saved_duplicates(request):
    """
    Look for any duplicates that exist among already-saved records in the
    database.
    """
    if request.method == "POST":
        try:
            wf1 = WorkerFatality.objects.get(pk=int(request.POST.get("pk1")))
            wf2 = WorkerFatality.objects.get(pk=int(request.POST.get("pk2")))
        except (ValueError, WorkerFatality.DoesNotExist):
            pass
        else:
            wf1.nonduplicates.add(wf2)
            wf2.nonduplicates.add(wf1)
            just_updated = [wf1, wf2]
    else:
        just_updated = None

    # Naive strategy -- (n+1) queries
    dup_pairs = set()
    possible_dups = []
    empty_penalty = 0.8
    max_distance = 1.9
    has_more = True
    for wf in WorkerFatality.objects.select_related():
        qs = WorkerFatality.objects.find_potential_duplicates(
                wf, empty_penalty=empty_penalty, max_dist=max_distance)
        for dup in qs:
            # Check that this pairing isn't part of an already proposed
            # duplicate. This is to prevent returning both (a,b) and (b,a) as
            # duplicates.
            pair = frozenset((wf.id, dup.id))
            if pair in dup_pairs:
                continue
            dup_pairs.add(pair)
            similarity = int((1 - dup.score / max_distance) * 100) or 1
            possible_dups.append((similarity, (wf, dup)))
        if len(possible_dups) >= 20:
            break
    else:
        has_more = False

    possible_dups.sort(reverse=True, key=lambda o: o[0])
    context = {
        "possible_dups": possible_dups,
        "just_updated": just_updated,
        "has_more": has_more
    }
    context.update(admin.site.each_context(request))
    return render(request, "admin/check-saved-duplicates.html", context)

@user_passes_test(staff_check)
def merge_worker_fatalities(request):
    row = None
    upload = None
    try:
        wf1 = WorkerFatality.objects.get(id=int(request.GET.get("pk1")))
    except (ValueError, WorkerFatality.DoesNotExist):
        raise Http404
    if "pk2" in request.GET:
        try:
            wf2 = WorkerFatality.objects.get(id=int(request.GET.get("pk2")))
        except (ValueError, WorkerFatality.DoesNotExist):
            raise Http404
    elif "upload" in request.GET and "row" in request.GET:
        try:
            upload = UploadInProgress.objects.get(id=int(request.GET.get("upload")))
        except (ValueError, UploadInProgress.DoesNotExist):
            raise Http404
        try:
            row = int(request.GET.get('row'))
            entry = [e for e in upload.pending_entries if e['row'] == row][0]
        except (IndexError, ValueError):
            raise Http404
        wf2 = WorkerFatality(**PendingEntry.from_dict(entry).clean_attrs())
        wf2.m2m_dict = entry['m2m']

    form = forms.MergeRecordsForm(
            request.POST or None,
            request.FILES or None,
            instance=wf1,
            comparison=wf2)
    if form.is_valid():
        form.save()
        if wf2.pk:
            wf2.delete()
            return redirect("check_saved_duplicates")
        elif row and upload:
            upload.pending_entries = [e for e in upload.pending_entries if e['row'] != row]
            upload.save()
            return redirect(upload.get_absolute_url())

    context = {
        'form': form,
        'wf1': wf1,
        'wf2': wf2,
        'row': row,
        'upload': upload
    }
    context.update(admin.site.each_context(request))
    return render(request, "admin/merge-worker-fatalities.html", context)

@user_passes_test(staff_check)
def export_worker_fatalities(request, fmt="csv", encoding="utf-8", qs=None):
    fields = ['date_created', 'first_name', 'last_name', 'age',
            'sex', 'date_event', 'date_death', 'event', 'event_city',
            'event_state', 'occupation', 'industry', 'immigrant',
            'affiliation', 'exposure', 'origin_country', 'origin_city',
            'origin_state', 'direct_employer', 'collector', 'photo']
    m2m_fields = ['contracts', 'sources']
    max_m2m_count = 3

    heading = [] + fields 
    for field in m2m_fields:
        heading += [field + "_{}".format(i + 1) for i in range(max_m2m_count)]

    data = []
    if qs is None:
        qs = WorkerFatality.objects.all().select_related().order_by('date_created')
    else:
        qs = qs.select_related()
    for wf in qs:
        row = []
        for field in fields:
            val = getattr(wf, field)
            # datetime.datetime first, as datetime.datetime is instance of datetime.date
            if isinstance(val, datetime.datetime):
                out = val.isoformat().split(".")[0] + "Z"
            elif isinstance(val, datetime.date):
                out = val.isoformat()
            elif val is None:
                out = ""
            else:
                out = str(val)
            row.append(out)
        for field in m2m_fields:
            vals = getattr(wf, field).all()[0:max_m2m_count]
            for i in range(max_m2m_count):
                if i < len(vals):
                    row.append(str(vals[i]))
                else:
                    row.append(None)
        data.append(row)

    if fmt == "csv":
        sio = StringIO()
        writer = csv.writer(sio)
        writer.writerow(heading)
        for row in data:
            writer.writerow([("" if c is None else c) for c in row])
        response = HttpResponse()
        response['Content-Type'] = "text/csv; charset={}".format(encoding)
        response['Content-Disposition'] = "attachment; filename={}".format(
            datetime.datetime.now().strftime("workerfatalities-%Y-%m-%d.csv")
        )
        response.write(sio.getvalue().encode(encoding))
    elif fmt == "xlsx":
        wb = Workbook()
        ws = wb.active
        ws.append(heading)
        for row in data:
            ws.append(row)
        response = HttpResponse()
        response['Content-Type'] = 'application/vnd.ms-excel'
        response['Content-Disposition'] = "attachment; filename={}".format(
            datetime.datetime.now().strftime("workerfatalities-%Y-%m-%d.xlsx")
        )
        response.write(save_virtual_workbook(wb))
    elif fmt == "json":
        response = JsonResponse({
            'worker_fatalities': [dict(zip(heading, row)) for row in data]
        })
    return response

@xframe_options_exempt
def public_form(request):
    collector = Collector.objects.get_or_create(
            name='Anonymous public')[0]
    form = forms.SingleEntryForm(request.POST or None, 
            request.FILES or None,
            instance=WorkerFatality(collector=collector))
    if form.is_valid():
        form.save()
        return redirect("public_form_thanks")
    return render(request, "survey/public_form.html", {
      "form": form
    })

@xframe_options_exempt
def public_form_thanks(request):
    return render(request, "survey/public_form_thanks.html")

@xframe_options_exempt
@csrf_exempt
def duplicates_ajax(request):
    if not request.is_ajax():
        return HttpResponseBadRequest("AJAX only")
    if request.method != "POST":
        return HttpResponseBadRequest("POST only")

    form = forms.SingleEntryDuplicateCheck(request.POST)
    output_fields = [
        'id', 'first_name', 'last_name', 'event_city', 'event_state',
        'sex', 'age', 'immigrant', 'origin_city', 'origin_state',
        'origin_country', 'date_event', 'date_death', 'event',
        'score'
    ]
    duplicates = form.find_duplicates()

    return JsonResponse({'duplicates':
        list(map(lambda d: {k: getattr(d, k) for k in output_fields},
            duplicates[0:3]))
    }, encoder=utils.PendingEntryJSONEncoder)

