import logging
from django import template
from survey.models import SiteCopy
from django.utils.safestring import mark_safe

register = template.Library()

@register.simple_tag
def site_copy(key):
    try:
        return mark_safe(SiteCopy.objects.get(key=key).description)
    except SiteCopy.DoesNotExist:
        SiteCopy.objects.create(key=key, description="");
        logging.error("SiteCopy {} not found".format(key))
        return ""
