from django import template
register = template.Library()

@register.inclusion_tag("survey/harvey_balls.html")
def harvey_balls(one_to_one_hundred):
    return {'score': one_to_one_hundred}

