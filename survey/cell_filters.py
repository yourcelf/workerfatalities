import re
import datetime
import dateutil.parser
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.utils import timezone

from localflavor.us.us_states import STATES_NORMALIZED
from survey.models import (
    Affiliation, Collector, Contract, Employer, Exposure, Occupation, Source
)
from locality.models import Country
from naics.models import NAICSCode
from modeltranslation.utils import auto_populate

class CellException(Exception):
    pass

class TooLong(CellException):
    def __init__(self, value, max_length=255):
        super(TooLong, self).__init__("'{}' too long. Maximum length: {}".format(
            value, max_length))

def related_lookup(Model, value, lookup=None, default=None):
    if not value:
        return None
    try:
        return Model.objects.get(**lookup)
    except Model.DoesNotExist:
        if default:
            with auto_populate():
                return Model.objects.create(**default)
        raise CellException(_("Not an existing value for this field"))

def parse_date(value):
    if isinstance(value, str):
        value = value.strip()
    if value in ("", None):
        return None
    date = None
    if isinstance(value, (float, int)):
        # Not likely to be a timestamp, as it's too low.  Probbably a weird
        # excel date format that appears to count days since 1899-12-30.
        if value < 100000:
            date = datetime.date(1899, 12, 30) + datetime.timedelta(days=int(value))
    elif isinstance(value, str):
        for fmt in ("%Y-%m-%d","%m/%d/%Y","%m/%d/%y"):
            try:
                date = datetime.datetime.strptime(value, fmt).date()
            except ValueError:
                continue
            else:
                if date:
                    break
    if date is None:
        try:
            date = dateutil.parser.parse(value, fuzzy=True)
        except ValueError:
            raise CellException(_("Date format not understood"))
    if date and date.year < 1700:
        raise CellException(_("Date format not understood"))
    return date

def parse_date_to_datetime(value):
    if isinstance(value, str):
        value = value.strip()
    if value in ("", None):
        return None
    return timezone.make_aware(datetime.datetime(*parse_date(value).timetuple()[0:6]))

def filter_unknown(value):
    return {"unknown": ""}.get(str(value).lower(), value)

def int_or_none(value):
    if value is None:
        return None
    else:
        if isinstance(value, str):
            value = value.strip()
            if value.lower() in ("", "unknown"):
                return None
        try:
            return int(value)
        except (ValueError, TypeError):
            raise CellException(_("Value must be a number"))

def sex(value):
    value = str(value).strip()
    if not value:
        return None
    return {
        'male': 'male',
        'm': 'male',
        'female': 'female',
        'f': 'female',
    }.get(value.lower())

def choices(*args):
    def choices_value(value):
        value = str(value).strip()
        if not value:
            return None
        if value in args:
            return value
        else:
            raise CellException(_("Not a valid value for this field"))
    return choices_value

def state(value):
    value = str(value).strip()
    if not value:
        return ""
    else:
        clean = re.sub("[^a-z]", " ", value.lower()).strip()
        result = STATES_NORMALIZED.get(clean, "")
        if not result:
            raise CellException(_("US State name not recognized"))
        return result

def yepnope(value):
    value = str(value).strip()
    if not value:
        return None
    if value.lower() in ["yes", "y", "true", "1"]:
        return True
    if value.lower() in ["no", "n", "false", "0"]:
        return False

def naics(value):
    value = str(value).strip()
    if not value or value == "other":
        return None
    if re.match("^naics_.*$", value):
        code = value.replace("naics_", "").replace("_", "-")
    elif re.match("^\d+/.*$", value):
        code = value.split("/")[0]
    elif re.match("^[-\d]+\s-\s.*$", value):
        code = value.split(" - ")[0]
    elif re.match("^\d+(-\d+)?$", value):
        code = value
    else:
        raise CellException("'{}' not recognized as an industry code".format(value))
    return related_lookup(NAICSCode, value, {"code": code})

def source(value):
    try:
        obj = Source.objects.build_from_value(value)
    except ValueError:
        raise TooLong(value, 255)
    if obj and not obj.pk:
        obj.save()
    return obj

def country(value):
    value = str(value).strip()
    return related_lookup(Country, value, {"name__iexact": value})

def occupation(value):
    value = str(value).strip()
    if len(value) > 255:
        raise TooLong(value, 255)
    return related_lookup(Occupation, value, {"name_en__iexact": value}, {"name": value})

def affiliation(value):
    value = str(value).strip()
    return related_lookup(Affiliation, value, {"name_en__iexact": value})

def exposure(value):
    value = str(value).strip()
    if value == "unknown":
        return None
    return related_lookup(Exposure, value, {"name_en__iexact": value})

def employer(value):
    value = str(value).strip()
    if len(value) > 255:
        raise TooLong(value, 255)
    return related_lookup(Employer, value, {"name__iexact": value}, {"name": value})

def contract(value):
    value = str(value).strip()
    if len(value) > 255:
        raise CellException(_("Value too long, maximum length 255 characters"))
    return related_lookup(Contract, value, {"name__iexact": value}, {"name": value})

def file_from_url(value):
    pass

def collector(value):
    value = str(value).strip()
    return related_lookup(Collector, value, {"name__iexact": value})

class m2m:
    def __init__(self, field_name):
        self.field_name = field_name

    def __str__(self):
        return self.field_name
