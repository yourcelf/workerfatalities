import subprocess
from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.conf import settings

def psql(query):
    return """sudo su {dbsuper} -c "psql -c \\"%s\\"" """ % query

def psqld(query):
    return """sudo su {dbsuper} -c "psql -d {dbname} -c \\"%s\\"" """ % query

steps = [
    {"cmd": psql("DROP DATABASE IF EXISTS {dbname}")},
    {"cmd": psql("DROP USER IF EXISTS {dbuser}")},
    {"cmd": psql("CREATE USER {dbuser} WITH {createdb} {superuser} PASSWORD '{dbpass}'")},
    {"cmd": "sudo su {dbsuper} -c 'createdb {dbname} -O {dbuser}'"},
    {"cmd": psqld("CREATE EXTENSION pg_trgm;")},
    {"management": "migrate"},
    {"management": "loaddata", "args": ["locality.json", "naics.json"]},
]

class Command(BaseCommand):
    help = 'Reset the database'

    def add_arguments(self, parser):
        parser.add_argument("--postgres-superuser", nargs='?', default="postgres")
        parser.add_argument("--noinput", action='store_true')
        parser.add_argument("--createdb", action='store_true')
        parser.add_argument("--superuser", action='store_true')

    def handle(self, *args, **options):
        if not options['noinput']:
            confirm = input(
                "Are you sure you want to IRREVERSIBLY destroy the database? [y/n] "
            )
            if confirm != "y":
                print("Aborting.")
                return

        cmd_vars = {
            'dbsuper': options['postgres_superuser'],
            'dbuser': settings.DATABASES['default']['USER'],
            'dbname': settings.DATABASES['default']['NAME'],
            'dbpass': settings.DATABASES['default']['PASSWORD'],
            'createdb': "CREATEDB" if options['createdb'] else "",
            'superuser': "SUPERUSER" if options['superuser'] else "",
        }
        for step in steps:
            if "cmd" in step:
                print(repr(step['cmd']))
                subprocess.check_call(step['cmd'].format(**cmd_vars), shell=True)
            elif "management" in step:
                call_command(step['management'], *step.get('args', []))
