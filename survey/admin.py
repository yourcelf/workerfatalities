from django.contrib import admin
from django.db.models import Q
from django.db.models.sql.where import EverythingNode
from django.db.models.lookups import In
from django.utils.encoding import smart_text
from survey.models import (
    Occupation, Affiliation, Exposure, Employer, WorkerFatality, Contract,
    Collector, Source, SiteCopy, UploadInProgress
)
from survey.views import export_worker_fatalities
from naics.models import NAICSCode


class NameOnlyAdmin(admin.ModelAdmin):
    search_fields = ['name']
    # The modeltranslation variants ([en], [es]) will be shown instead.
class TranslatedNameOnlyAdmin(NameOnlyAdmin):
    exclude = ['name']

admin.site.register(Occupation, TranslatedNameOnlyAdmin)
admin.site.register(Affiliation, TranslatedNameOnlyAdmin)
admin.site.register(Exposure, TranslatedNameOnlyAdmin)
admin.site.register(Employer, NameOnlyAdmin)
admin.site.register(Contract, NameOnlyAdmin)
admin.site.register(Collector, NameOnlyAdmin)

class SourceAdmin(admin.ModelAdmin):
    list_display = ['note', 'url']
    search_fields = ['note', 'url']
admin.site.register(Source, SourceAdmin)

class SourceInline(admin.TabularInline):
    model = Source

def remove_zero_id_constraint(queryset):
    for i,child in enumerate(queryset.query.where.children):
        if isinstance(child, In) and \
                str(child.lhs.field) == "survey.WorkerFatality.id" and \
                child.rhs == [0]:
            queryset.query.where.children[i] = EverythingNode()

class TopLevelIndustryListFilter(admin.RelatedFieldListFilter):
    title = "Industry"
    parameter_name = 'industry'

    def has_output(self):
        return True

    def choices(self, cl):
        yield {
            'selected': self.lookup_val is None,
            'query_string': cl.get_query_string({}, [self.lookup_kwarg]),
            'display': 'All'
        }
        for pk, code, description in NAICSCode.objects.top_level().values_list(
                'pk', 'code', 'description'):
            pk_str = smart_text(pk)
            yield {
                'selected': pk_str == self.lookup_val,
                'query_string': cl.get_query_string({
                    self.lookup_kwarg: pk_str
                }),
                'display': " - ".join((code, description))
            }
        yield {
            'selected': self.lookup_val == "null",
            'query_string': cl.get_query_string({self.lookup_kwarg: "null"}),
            'display': 'Unknown'
        }

    def queryset(self, request, queryset):
        if self.lookup_val == "null":
            return queryset.filter(industry__isnull=True)
        elif self.lookup_val is None:
            return queryset
        else:
            return queryset.filter(
                Q(industry__pk=self.lookup_val) |
                Q(industry__ancestor__pk=self.lookup_val)
            )

class BlankableChoicesFieldListFilter(admin.ChoicesFieldListFilter):
    def choices(self, cl):
        for choice in super(BlankableChoicesFieldListFilter, self).choices(cl):
            yield choice
        yield {
            'selected': self.lookup_val == "",
            'query_string': cl.get_query_string({self.lookup_kwarg: ""}),
            'display': 'Unknown'
        }

class WorkerFatalityAdmin(admin.ModelAdmin):
    list_display = ['full_name', 'date_created', 'date_death', 'event_city', 'event_state']
    date_hierarchy = 'date_death'
    list_filter = [
        'collector',
        ('date_modified', admin.DateFieldListFilter),
        'immigrant',
        ('sex', BlankableChoicesFieldListFilter),
        ('industry', TopLevelIndustryListFilter),
        ('affiliation', admin.RelatedOnlyFieldListFilter),
        ('exposure', admin.RelatedOnlyFieldListFilter),
        ('event_state', BlankableChoicesFieldListFilter),
    ]
    search_fields = [
        'last_name', 'first_name', 'event', 'event_city', 'event_state',
        'origin_state', 'origin_city', 'notes',
        'industry__description_en', 'industry__description_es',
        'affiliation__name_en', 'affiliation__name_es',
        'exposure__name_en', 'exposure__name_es',
        'direct_employer__name', 'contracts__name',
    ]
    filter_horizontal = ['contracts', 'sources', 'nonduplicates']
    change_list_template = "admin/download_spreadsheet_change_list.html"
    
    actions = ['download_csv', 'download_xlsx', 'download_json']

    def download_csv(self, request, queryset):
        remove_zero_id_constraint(queryset)
        return export_worker_fatalities(request, fmt='csv', qs=queryset)
    download_csv.short_description = "Export selected as csv"

    def download_xlsx(self, request, queryset):
        remove_zero_id_constraint(queryset)
        return export_worker_fatalities(request, fmt='xlsx', qs=queryset)
    download_xlsx.short_description = "Export selected as xlsx"

    def download_json(self, request, queryset):
        remove_zero_id_constraint(queryset)
        return export_worker_fatalities(request, fmt='json', qs=queryset)
    download_json.short_description = "Export selected as json"
admin.site.register(WorkerFatality, WorkerFatalityAdmin)

class SiteCopyAdmin(admin.ModelAdmin):
    list_display = ['key', 'description_en', 'description_es']
    search_fields = ['key', 'description_en', 'description_es']
    exclude = ['description']
    readonly_fields = ['key']
admin.site.register(SiteCopy, SiteCopyAdmin)


class UploadInProgressAdmin(admin.ModelAdmin):
    list_display = ['name', 'uploader', 'rows', 'created', 'modified', 'complete']
    list_filter = ['complete']
    readonly_fields = ['created', 'modified', 'complete', 'rows', 'saved_entries_count']
    exclude = ['pending_entries', 'saved_entries']

    def rows(self, obj):
        return len(obj.pending_entries)

    def saved_entries_count(self, obj):
        return obj.saved_entries.count()

admin.site.register(UploadInProgress, UploadInProgressAdmin)
