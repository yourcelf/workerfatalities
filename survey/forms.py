import os
import json
import string
import tempfile
from collections import defaultdict
from fuzzywuzzy import fuzz
from django import forms
from django.forms import widgets
from django.core.exceptions import ValidationError, FieldDoesNotExist
from django.db import transaction
from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.db import models
from survey.models import WorkerFatality, PendingEntry, Source, Contract, Employer
from survey import utils
from survey import cell_filters
from naics.models import NAICSCode

class UploadForm(forms.Form):
    file = forms.FileField(label=_("spreadsheet"))

    def clean_file(self):
        uf = self.cleaned_data.get('file')
        if uf:
            ext = os.path.splitext(uf.name)[1]
            with tempfile.NamedTemporaryFile(suffix=ext) as fh:
                for chunk in uf.chunks():
                    fh.write(chunk)
                fh.flush()
                try:
                    entries = utils.read_spreadsheet(fh.name)
                except utils.SheetException as e:
                    raise ValidationError(str(e))
                self.cleaned_data['pending_entries'] = entries

ADDABLE_RELATIONS = [
    "occupation", "affiliation", "exposure", "employer", "contract", "collector"
]
class AddNameOnlyRelation(forms.Form):
    field = forms.ChoiceField(choices=[(a,a) for a in ADDABLE_RELATIONS])
    value = forms.CharField()

    def clean_field(self):
        try:
            model = WorkerFatality._meta.get_field(
                    self.cleaned_data['field']).related_model()._meta.model
        except AttributeError:
            raise ValidationError(_("Invalid field"))
        return model 

    def clean(self):
        super(AddNameOnlyRelation, self).clean()
        value = str(self.cleaned_data['value']).strip()
        instance = self.cleaned_data['field'](name=value)
        instance.full_clean()
        self.cleaned_data['instance'] = instance
        return self.cleaned_data

    def add(self):
        self.cleaned_data['instance'].save()
        return self.cleaned_data['instance']

class EntryConfirmationForm(forms.Form):
    """
    Form to hold a description of all pending entries. 
    """
    pending_entries_json = forms.CharField(widget=forms.HiddenInput)

    def __init__(self, data=None, *args, entries=None, **kwargs):
        if data is None and entries is not None:
            self.entries = entries
            data = {
                "pending_entries_json": json.dumps([e.to_dict() for e in entries],
                                                   cls=utils.PendingEntryJSONEncoder)
            }
        elif data and 'pending_entries_json' in data:
            data = data.copy()
            entry_dicts = json.loads(data['pending_entries_json'])
            self.entries = [PendingEntry.from_dict(d) for d in entry_dicts]
        super(EntryConfirmationForm, self).__init__(data, *args, **kwargs)

    def clean_pending_entries_json(self):
        try:
            entries = json.loads(self.cleaned_data['pending_entries_json'])
        except ValueError:
            raise ValidationError(_("Form data is damaged, upload failed."))
        for entry_dict in entries:
            entry = PendingEntry.from_dict(entry_dict)
            if entry.errors:
                raise ValidationError(_("Form has errors."))
            elif len(entry.potential_duplicates) > 0:
                raise ValidationError(_("Form has potential duplicates."))
        return self.cleaned_data['pending_entries_json']

    def update_entry(self, params):
        action = params['action']

        # find the entry
        for entry in self.entries:
            if int(params['row']) == entry.row:
                break
        else:
            raise ValidationError("Entry not found")

        if action == "add":
            field_name = params['field']
            old_value = entry.fields[field_name]['value']
            form = AddNameOnlyRelation(params)
            if form.is_valid():
                replacement = form.add()
            else:
                raise ValidationError(str(form.errors))
            # For add, replace all entries with the new value, as the presumed
            # reason for invalidity of the old one is that the relation hadn't
            # been added.
            for entry in self.entries:
                if entry.fields[field_name]['value'] == old_value:
                    entry.replace_value(field_name, replacement)
                    entry.revalidate()
        elif action == "ignore":
            entry.replace_value(params['field'], None, params.get('value'))
            entry.revalidate()
        elif action == "replace":
            entry.replace_value(params['field'], params.get('replacement'), params.get('value'))
            entry.revalidate()
        elif action == "overwrite_db_entry":
            entry.duplicate_action['overwrite'] = params.get('id')
            entry.revalidate()
        elif action == "discard_row":
            self.entries.remove(entry)
            entry.revalidate()
        elif action == "nonduplicates":
            entry.duplicate_action['nonduplicates'] = entry.duplicate_action.get('nonduplicates', []) + [params.get('id')]
            entry.revalidate()
        else:
            raise ValidationError("Unknown action")

        # Refresh pending_entries_json
        self.data['pending_entries_json'] = json.dumps(
            [e.to_dict() for e in self.entries],
            cls=utils.PendingEntryJSONEncoder)

    def save(self):
        models = []
        with transaction.atomic():
            for entry in self.entries:
                models.append(entry.save())
        return models


    def field_info(self, field_name):
        try:
            Field = WorkerFatality._meta.get_field(field_name)
        except FieldDoesNotExist:
            return None
        ftype = None
        if isinstance(Field, (models.DateField, models.DateTimeField)):
            ftype = "date"
        elif isinstance(Field, models.ForeignKey):
            ftype = "related"
        elif isinstance(Field, models.ManyToManyField):
            ftype = "m2m"
        elif isinstance(Field, (models.BooleanField, models.NullBooleanField)):
            ftype = "boolean"
        elif isinstance(Field, models.IntegerField):
            ftype = "int"
        elif isinstance(Field, models.EmailField):
            ftype = "email"
        elif isinstance(Field, (models.CharField, models.TextField)):
            ftype = "string"
        elif isinstance(Field, (models.AutoField,)):
            ftype = "readonly"
        else:
            raise Exception("Unhandled field type {}".format(type(Field)))

        choices = None
        if ftype == "related" or Field.choices:
            choices = Field.get_choices()

        return {
            'constraints': {
                'type': ftype,
                'choices': choices,
                'blank': Field.blank,
                'null': Field.null,
                'max_length': getattr(Field, "max_length", None),
            },
            'verbose_name': string.capwords(Field.verbose_name)
        }

    def duplicates_dict(self):
        """
        Return a JSON structure with the components of potential duplicates we
        want to show, for consumption by a client-side script to warn about
        them.
        """
        # Return basically all fields, so we can render a "merge records" view.
        data = {
            "heading": [
                "id", "first_name", "last_name", "age", "sex", "date_event",
                "date_death", "event", "event_city", "event_state",
                "occupation", "industry", "immigrant", "affiliation",
                "exposure", "origin_country", "origin_city", "origin_state",
                "direct_employer", "contracts", "sources", "notes", "email",
                "admin_url", "score"
            ],
            "fields": {},
            "rows": defaultdict(lambda: {'entry': None, 'duplicates': []})
        }
        for field_name in data['heading']:
            data['fields'][field_name] = self.field_info(field_name)

        for entry in self.entries:
            if not entry.errors and entry.potential_duplicates:
                # Grab the entry itself (what is duplicated?).
                duplicate_set = data['rows'][str(entry.row)]
                duplicate_set['entry'] = [
                    entry.fields.get(key, {}).get('value', '') for key in data['heading']
                ]
                # Grab the duplicates.
                for model in entry.potential_duplicates:
                    serialized = []
                    for field_name in data['heading']:
                        value = getattr(model, field_name)
                        if isinstance(value, models.Manager):
                            value = list(value.all())
                        serialized.append(value)
                    duplicate_set['duplicates'].append(serialized)
        data['rows'] = dict(data['rows'])
        return data

    def errors_dict(self):
        """
        Return a JSON structure that is just the errors, compactly represented,
        for consumption by a client-side script to create a form.
        """
        data = {
            "heading": ["field_name", "row", "col", "value", "error", "action_choices"],
            "fields": {},
            "data": [],
        }
        for entry in self.entries:
            for field_name, field_desc in entry.errors.items():
                if field_name not in data['fields']:
                    data['fields'][field_name] = self.field_info(field_name)

                action_choices = [
                    ["replace", _("Change the value")],
                ]
                if data['fields'][field_name]['constraints']['blank']:
                    action_choices.append(
                        ["ignore", _("Ignore this field (it will be left blank)")]
                    )
                if field_name in ADDABLE_RELATIONS:
                    action_choices.append(
                        ["add", _("Add \"%(value)s\" as a choice for %(field)s") % {
                            "value": field_desc['value'],
                            "field": data['fields'][field_name]['verbose_name']
                        }]
                    )

                if isinstance(field_desc, list):
                    # m2m: just show one error at a time.
                    all_descs = field_desc
                else:
                    # regular
                    all_descs = [field_desc]
                for field_desc in all_descs:
                    data['data'].append([
                        field_name,
                        field_desc['cell']['row'],
                        field_desc['cell']['col'],
                        field_desc['value'],
                        "\n".join(field_desc['errors']),
                        action_choices,
                    ])
        # Sort by row/column
        data['data'].sort(key=lambda e: (e[2], e[3]))
        return data

    def data_dict(self):
        return {
            "errors": self.errors_dict(),
            "duplicates": self.duplicates_dict(),
        }

    def data_json(self):
        return json.dumps(self.data_dict(), cls=utils.PendingEntryJSONEncoder)

class ValueChoiceSelectMultiple(forms.SelectMultiple):
    def render(self, name, value, attrs=None, choices=()):
        choices = self.choices(value)
        if value is None:
            value = ''
        final_attrs = self.build_attrs(attrs, name=name)
        output = [format_html('<select multiple {}>', forms.utils.flatatt(final_attrs))]
        for value, display in choices:
            output.append(format_html(
                "<option value='{}' selected='selected'>{}</option>",
                value,
                display))
        output.append('</select>')
        return mark_safe('\n'.join(output))

class CreatableM2MField(forms.ModelMultipleChoiceField):
    """A MultipleChoiceField which can tolerate creation of new values"""
    new_prefix = "new-"
    widget = ValueChoiceSelectMultiple

    def _get_choices(self):
        """
        Return a closure which can vivify models for choices.
        """
        def choice_getter(value):
            models = self._check_values(value)
            return [(self.prepare_value(m), str(m)) for m in models]
        return choice_getter
    _choices = property(_get_choices)

    def prepare_value(self, value):
        if (hasattr(value, '__iter__') and
                not isinstance(value, str) and
                not hasattr(value, '_meta')):
            return [self.prepare_value(v) for v in value]
        if hasattr(value, '_meta'):
            if value.pk is None:
                return "{}{}".format(self.new_prefix, str(value))
            return value.pk
        return value

    def _check_values(self, value):
        Model = self.queryset.model
        new_values = []
        pks = []
        for arg in value:
            if str(arg).startswith(self.new_prefix):
                new_obj = Model.objects.build_from_value(arg[len(self.new_prefix):])
                new_values.append(new_obj)
            else:
                pks.append(arg)
        existing_values = list(Model.objects.filter(pk__in=pks))
        return existing_values + new_values

class DashedNullBooleanSelect(widgets.NullBooleanSelect):
    """
    Override null boolean select to use dashes instead of "Unknown".
    """
    def __init__(self, attrs=None):
        choices = (('1', '---------'),
                   ('2', _('Yes')),
                   ('3', _('No')))
        # Skip parent constructor, which sets 'choices'. call super on our
        # parent
        super(widgets.NullBooleanSelect, self).__init__(attrs, choices)

class BaseSingleEntryForm(forms.ModelForm):
    age = forms.CharField(
            widget=forms.TextInput,
            required=False,
            label=WorkerFatality._meta.get_field('age').verbose_name)
    contracts = CreatableM2MField(
            queryset=Contract.objects.none(),
            label=WorkerFatality._meta.get_field('contracts').verbose_name,
            help_text=WorkerFatality._meta.get_field('contracts').help_text,
            required=False)
    sources = CreatableM2MField(
            queryset=Source.objects.none(),
            required=False,
            label=WorkerFatality._meta.get_field('sources').verbose_name,
            help_text=WorkerFatality._meta.get_field('sources').help_text)
    direct_employer = forms.CharField(
            label=WorkerFatality._meta.get_field('direct_employer').verbose_name,
            help_text=WorkerFatality._meta.get_field('direct_employer').help_text,
            required=False)
    industry = forms.ModelChoiceField(
            queryset=NAICSCode.objects.top_level(),
            label=WorkerFatality._meta.get_field('industry').verbose_name,
            help_text=WorkerFatality._meta.get_field('industry').help_text,
            required=False)
    immigrant = forms.NullBooleanField(
            widget=DashedNullBooleanSelect,
            label=WorkerFatality._meta.get_field("immigrant").verbose_name,
            help_text=WorkerFatality._meta.get_field('immigrant').help_text,
            required=False)

    class Meta:
        model = WorkerFatality
        exclude = ['date_created', 'date_modified', 'collector', 'nonduplicates']

    def clean_direct_employer(self):
        val = self.cleaned_data.get('direct_employer')
        if val:
            try:
                return Employer.objects.get(name__iexact=val)
            except Employer.DoesNotExist:
                return Employer(name=val)

    def clean_age(self):
        if self.cleaned_data.get('age'):
            try:
                return int(self.cleaned_data.get('age'))
            except ValueError:
                raise ValidationError("Age must be a number")
        return None

    def save(self, commit=True):
        instance = super(BaseSingleEntryForm, self).save(commit=False)
        if commit:
            employer = instance.direct_employer
            if employer and not employer.pk:
                employer.save()
                instance.direct_employer = employer
            for field in ["contracts", "sources"]:
                for model in self.cleaned_data.get(field, []):
                    if model.pk is None:
                        model.save()
            instance.save()
            self.save_m2m()
        return instance

class SingleEntryForm(BaseSingleEntryForm):
    def __init__(self, *args, **kwargs):
        super(SingleEntryForm, self).__init__(*args, **kwargs)
        required = ["date_event", "event", "event_state", "sources"]
        for field_name in required:
            self.fields[field_name].required = True

class SingleEntryDuplicateCheck(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(SingleEntryDuplicateCheck, self).__init__(*args, **kwargs)

    def find_duplicates(self):
        if self.is_valid():
            pe = PendingEntry()
            for key, val in self.cleaned_data.items():
                pe.fields[key] = {'value': val}
            return WorkerFatality.objects.find_potential_duplicates(pe)
        return WorkerFatality.objects.none()

    class Meta:
        model = WorkerFatality
        fields = ['first_name', 'last_name', 'event_city', 'event_state',
                'sex', 'age', 'immigrant', 'origin_city', 'origin_state',
                'origin_country', 'date_event', 'date_death']

class MergeRecordsForm(BaseSingleEntryForm):
    industry = forms.ModelChoiceField(
            queryset=NAICSCode.objects.all(), # instead of 'top_level' in parent
            label=WorkerFatality._meta.get_field('industry').verbose_name,
            help_text=WorkerFatality._meta.get_field('industry').help_text,
            required=False)

    def __init__(self, *args, **kwargs):
        instance = kwargs['instance']
        comparison = kwargs.pop('comparison')
        super(MergeRecordsForm, self).__init__(*args, **kwargs)

        self.fields['sources'].queryset = Source.objects.filter(
            id__in=list(instance.sources.values_list('pk', flat=True))
        )
        self.fields['sources'].initial = list(instance.sources.all())

        self.fields['contracts'].queryset = Contract.objects.filter(
            id__in=list(instance.contracts.values_list('pk', flat=True))
        )
        self.fields['contracts'].initial = list(instance.contracts.all())

        if instance.direct_employer:
            self.fields['direct_employer'].initial = str(instance.direct_employer)
        else:
            self.fields['direct_employer'].initial = ""
        self.initial['direct_employer'] = self.fields['direct_employer'].initial

        for field in self.visible_fields():
            v1 = getattr(instance, field.name)
            try:
                v2 = getattr(comparison, field.name)
            except ValueError:
                values = comparison.m2m_dict.get(field.name, [])
                v2 = ", ".join([str(m['value']) for m in values])

            # handle m2m's
            if isinstance(v2, models.Manager):
                field.other = ", ".join((str(m) for m in v2.all()))
            elif v2 is None:
                field.other = "-"
            else:
                field.other = str(v2)
