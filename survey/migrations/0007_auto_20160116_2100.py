# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0006_auto_20160110_2012'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workerfatality',
            name='event',
            field=models.TextField(blank=True, verbose_name='event description', help_text='Please describe the event in detail.'),
        ),
    ]
