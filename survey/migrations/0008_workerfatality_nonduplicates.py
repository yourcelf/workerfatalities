# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0007_auto_20160116_2100'),
    ]

    operations = [
        migrations.AddField(
            model_name='workerfatality',
            name='nonduplicates',
            field=models.ManyToManyField(blank=True, to='survey.WorkerFatality', related_name='_workerfatality_nonduplicates_+'),
        ),
    ]
