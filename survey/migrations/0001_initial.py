# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import localflavor.us.models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('naics', '0001_initial'),
        ('locality', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Affiliation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(unique=True, verbose_name='name', max_length=255)),
            ],
            options={
                'verbose_name_plural': 'affiliations',
                'verbose_name': 'affiliation',
            },
        ),
        migrations.CreateModel(
            name='Collector',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(unique=True, verbose_name='name', max_length=255)),
            ],
            options={
                'verbose_name_plural': 'collectors',
                'verbose_name': 'collector',
            },
        ),
        migrations.CreateModel(
            name='Contract',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(unique=True, verbose_name='name', max_length=255)),
            ],
            options={
                'verbose_name_plural': 'contracts',
                'verbose_name': 'contract',
            },
        ),
        migrations.CreateModel(
            name='Employer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(unique=True, verbose_name='name', max_length=255)),
            ],
            options={
                'verbose_name_plural': 'employers',
                'verbose_name': 'employer',
            },
        ),
        migrations.CreateModel(
            name='Exposure',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(unique=True, verbose_name='name', max_length=255)),
            ],
            options={
                'verbose_name_plural': 'exposures',
                'verbose_name': 'exposure',
            },
        ),
        migrations.CreateModel(
            name='Occupation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(unique=True, verbose_name='name', max_length=255)),
            ],
            options={
                'verbose_name_plural': 'occupations',
                'verbose_name': 'occupation',
            },
        ),
        migrations.CreateModel(
            name='Source',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('url', models.URLField(blank=True, verbose_name='URL', default='')),
                ('note', models.CharField(blank=True, verbose_name='note', max_length=255, default='')),
            ],
            options={
                'verbose_name_plural': 'sources',
                'verbose_name': 'source',
            },
        ),
        migrations.CreateModel(
            name='WorkerFatality',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='date created')),
                ('date_modified', models.DateTimeField(verbose_name='date modified', auto_now=True)),
                ('last_name', models.CharField(verbose_name='last name', max_length=255, blank=True)),
                ('first_name', models.CharField(verbose_name='first name', max_length=255, blank=True)),
                ('age', models.IntegerField(blank=True, null=True)),
                ('sex', models.CharField(blank=True, choices=[('male', 'male'), ('female', 'female')], max_length=255, verbose_name='sex')),
                ('date_event', models.DateField(verbose_name='date of event')),
                ('date_death', models.DateField(verbose_name='date of death')),
                ('event', models.TextField(verbose_name='event description')),
                ('event_city', models.CharField(verbose_name='event city', max_length=255)),
                ('event_state', localflavor.us.models.USPostalCodeField(choices=[('AL', 'Alabama'), ('AK', 'Alaska'), ('AS', 'American Samoa'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('AA', 'Armed Forces Americas'), ('AE', 'Armed Forces Europe'), ('AP', 'Armed Forces Pacific'), ('CA', 'California'), ('CO', 'Colorado'), ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FM', 'Federated States of Micronesia'), ('FL', 'Florida'), ('GA', 'Georgia'), ('GU', 'Guam'), ('HI', 'Hawaii'), ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'), ('LA', 'Louisiana'), ('ME', 'Maine'), ('MH', 'Marshall Islands'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'), ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'), ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'), ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('MP', 'Northern Mariana Islands'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'), ('PW', 'Palau'), ('PA', 'Pennsylvania'), ('PR', 'Puerto Rico'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'), ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VI', 'Virgin Islands'), ('VA', 'Virginia'), ('WA', 'Washington'), ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming')], max_length=2, verbose_name='event state')),
                ('immigrant', models.NullBooleanField(default=None, verbose_name='immigrant')),
                ('origin_city', models.CharField(verbose_name='origin city', max_length=255, blank=True)),
                ('origin_state', localflavor.us.models.USPostalCodeField(blank=True, choices=[('AL', 'Alabama'), ('AK', 'Alaska'), ('AS', 'American Samoa'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('AA', 'Armed Forces Americas'), ('AE', 'Armed Forces Europe'), ('AP', 'Armed Forces Pacific'), ('CA', 'California'), ('CO', 'Colorado'), ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FM', 'Federated States of Micronesia'), ('FL', 'Florida'), ('GA', 'Georgia'), ('GU', 'Guam'), ('HI', 'Hawaii'), ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'), ('LA', 'Louisiana'), ('ME', 'Maine'), ('MH', 'Marshall Islands'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'), ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'), ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'), ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('MP', 'Northern Mariana Islands'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'), ('PW', 'Palau'), ('PA', 'Pennsylvania'), ('PR', 'Puerto Rico'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'), ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VI', 'Virgin Islands'), ('VA', 'Virginia'), ('WA', 'Washington'), ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming')], max_length=2, verbose_name='origin state')),
                ('photo', models.ImageField(blank=True, upload_to='images/%Y/%m/%d', verbose_name='photo', null=True)),
                ('affiliation', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='affiliation', to='survey.Affiliation', blank=True, null=True)),
                ('collector', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='collector', to='survey.Collector', blank=True, null=True)),
                ('contracts', models.ManyToManyField(to='survey.Contract', blank=True, verbose_name='contracts')),
                ('direct_employer', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='direct employer', to='survey.Employer', blank=True, null=True)),
                ('exposure', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='exposure', to='survey.Exposure', blank=True, null=True)),
                ('industry', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='industry', to='naics.NAICSCode', blank=True, null=True)),
                ('occupation', models.ForeignKey(verbose_name='occupation', to='survey.Occupation', blank=True, null=True)),
                ('origin_country', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='origin country', to='locality.Country', blank=True, null=True)),
                ('sources', models.ManyToManyField(to='survey.Source', blank=True, verbose_name='sources')),
            ],
            options={
                'verbose_name_plural': 'worker fatalities',
                'verbose_name': 'worker fatality',
            },
        ),
    ]
