# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0005_auto_20160110_1715'),
    ]

    operations = [
        migrations.AddField(
            model_name='affiliation',
            name='name_en',
            field=models.CharField(verbose_name='name', max_length=255, null=True, unique=True),
        ),
        migrations.AddField(
            model_name='affiliation',
            name='name_es',
            field=models.CharField(verbose_name='name', max_length=255, null=True, unique=True),
        ),
        migrations.AddField(
            model_name='exposure',
            name='name_en',
            field=models.CharField(verbose_name='name', max_length=255, null=True, unique=True),
        ),
        migrations.AddField(
            model_name='exposure',
            name='name_es',
            field=models.CharField(verbose_name='name', max_length=255, null=True, unique=True),
        ),
        migrations.AddField(
            model_name='occupation',
            name='name_en',
            field=models.CharField(verbose_name='name', max_length=255, null=True, unique=True),
        ),
        migrations.AddField(
            model_name='occupation',
            name='name_es',
            field=models.CharField(verbose_name='name', max_length=255, null=True, unique=True),
        ),
    ]
