# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0003_auto_20160103_2229'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workerfatality',
            name='origin_state',
            field=models.CharField(help_text='What state was the worker originally from?', max_length=255, verbose_name='origin state', blank=True),
        ),
    ]
