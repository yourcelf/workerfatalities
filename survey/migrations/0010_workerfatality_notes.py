# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0009_auto_20160123_2356'),
    ]

    operations = [
        migrations.AddField(
            model_name='workerfatality',
            name='notes',
            field=models.TextField(blank=True, verbose_name='Additional information', help_text='Please include any additional comments or info about this.'),
        ),
    ]
