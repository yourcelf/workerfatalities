# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.contrib.postgres.operations import CreateExtension


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0001_initial'),
    ]

    operations = [
        CreateExtension("pg_trgm"),
    ] + [
        migrations.RunSQL(
            "CREATE INDEX trgm_idx_{field} ON survey_workerfatality USING gin({field} gin_trgm_ops)".format(field=field),
            "DROP INDEX trgm_idx_{field}".format(field=field)
        ) for field in ("first_name", "last_name", "event_city", "origin_city")
    ]
