# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import localflavor.us.models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0004_auto_20160109_0007'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workerfatality',
            name='affiliation',
            field=models.ForeignKey(verbose_name="Deceased worker's affiliation", null=True, blank=True, on_delete=django.db.models.deletion.SET_NULL, to='survey.Affiliation'),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='age',
            field=models.IntegerField(verbose_name="Deceased worker's age", blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='collector',
            field=models.ForeignKey(verbose_name='collector of this record', null=True, blank=True, on_delete=django.db.models.deletion.SET_NULL, to='survey.Collector'),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='contracts',
            field=models.ManyToManyField(verbose_name='Contracts under which worker worked', blank=True, to='survey.Contract'),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='direct_employer',
            field=models.ForeignKey(verbose_name="Worker's direct employer", null=True, blank=True, on_delete=django.db.models.deletion.SET_NULL, to='survey.Employer'),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='event_city',
            field=models.CharField(verbose_name='event city', blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='event_state',
            field=localflavor.us.models.USPostalCodeField(verbose_name='event state', blank=True, choices=[('AL', 'Alabama'), ('AK', 'Alaska'), ('AS', 'American Samoa'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('AA', 'Armed Forces Americas'), ('AE', 'Armed Forces Europe'), ('AP', 'Armed Forces Pacific'), ('CA', 'California'), ('CO', 'Colorado'), ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FM', 'Federated States of Micronesia'), ('FL', 'Florida'), ('GA', 'Georgia'), ('GU', 'Guam'), ('HI', 'Hawaii'), ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'), ('LA', 'Louisiana'), ('ME', 'Maine'), ('MH', 'Marshall Islands'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'), ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'), ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'), ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('MP', 'Northern Mariana Islands'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'), ('PW', 'Palau'), ('PA', 'Pennsylvania'), ('PR', 'Puerto Rico'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'), ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VI', 'Virgin Islands'), ('VA', 'Virginia'), ('WA', 'Washington'), ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming')], max_length=2),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='exposure',
            field=models.ForeignKey(verbose_name='Risk worker was exposed to', null=True, blank=True, on_delete=django.db.models.deletion.SET_NULL, to='survey.Exposure'),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='first_name',
            field=models.CharField(verbose_name="Deceased worker's first name", blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='immigrant',
            field=models.NullBooleanField(verbose_name='Was the worker an immigrant?', default=None),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='last_name',
            field=models.CharField(verbose_name="Deceased worker's last name", blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='occupation',
            field=models.ForeignKey(verbose_name="Worker's occupation", null=True, blank=True, to='survey.Occupation'),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='origin_city',
            field=models.CharField(verbose_name="Worker's origin city", blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='origin_country',
            field=models.ForeignKey(verbose_name="Worker's origin country", null=True, blank=True, on_delete=django.db.models.deletion.SET_NULL, to='locality.Country'),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='origin_state',
            field=models.CharField(verbose_name="Worker's origin state", blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='photo',
            field=models.ImageField(verbose_name='Photo of deceased', upload_to='images/%Y/%m/%d', blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='sex',
            field=models.CharField(verbose_name="Deceased worker's sex", blank=True, choices=[('male', 'male'), ('female', 'female')], max_length=255),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='sources',
            field=models.ManyToManyField(verbose_name='Sources of information about worker', blank=True, to='survey.Source'),
        ),
    ]
