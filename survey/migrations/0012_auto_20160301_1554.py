# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0011_auto_20160128_2255'),
    ]

    operations = [
        migrations.AlterField(
            model_name='source',
            name='url',
            field=models.TextField(verbose_name='URL', default='', validators=[django.core.validators.URLValidator()], blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='direct_employer',
            field=models.ForeignKey(verbose_name="Worker's direct employer", help_text='Who was paying the worker?', blank=True, to='survey.Employer', on_delete=django.db.models.deletion.SET_NULL, null=True),
        ),
    ]
