# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0008_workerfatality_nonduplicates'),
    ]

    operations = [
        migrations.CreateModel(
            name='SiteCopy',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('key', models.CharField(max_length=255, help_text='Machine name for this component')),
                ('description', models.TextField()),
                ('description_en', models.TextField(null=True)),
                ('description_es', models.TextField(null=True)),
            ],
        ),
        migrations.AlterModelOptions(
            name='affiliation',
            options={'verbose_name_plural': 'affiliations', 'verbose_name': 'affiliation', 'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='collector',
            options={'verbose_name_plural': 'collectors', 'verbose_name': 'collector', 'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='contract',
            options={'verbose_name_plural': 'contracts', 'verbose_name': 'contract', 'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='employer',
            options={'verbose_name_plural': 'employers', 'verbose_name': 'employer', 'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='exposure',
            options={'verbose_name_plural': 'exposures', 'verbose_name': 'exposure', 'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='occupation',
            options={'verbose_name_plural': 'occupations', 'verbose_name': 'occupation', 'ordering': ['name']},
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='industry',
            field=models.ForeignKey(help_text="Please choose an industry code that best corresponds to the worker's occupation.", to='naics.NAICSCode', null=True, blank=True, on_delete=django.db.models.deletion.SET_NULL, verbose_name='Industry'),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='nonduplicates',
            field=models.ManyToManyField(to='survey.WorkerFatality', related_name='_workerfatality_nonduplicates_+', blank=True, verbose_name='Non-duplicates', help_text='These other entries have been ruled out as duplicates'),
        ),
    ]
