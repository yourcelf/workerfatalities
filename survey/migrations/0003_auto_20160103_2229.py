# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import localflavor.us.models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0002_fulltext_indexes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workerfatality',
            name='affiliation',
            field=models.ForeignKey(verbose_name='affiliation', help_text='How was the worker affiliated with the employer?', to='survey.Affiliation', null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='age',
            field=models.IntegerField(verbose_name='age', help_text="What was the deceased worker's age?", blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='collector',
            field=models.ForeignKey(verbose_name='collector', help_text='Who collected this information?', to='survey.Collector', null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='contracts',
            field=models.ManyToManyField(verbose_name='contracts', to='survey.Contract', help_text='Under what contracts was the worker working?', blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='date_death',
            field=models.DateField(verbose_name='date of death', help_text='When did the worker die?', blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='date_event',
            field=models.DateField(verbose_name='date of event', help_text="When did the event which resulted in the worker's death occur?", blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='direct_employer',
            field=models.ForeignKey(verbose_name='direct employer', help_text="Who was the worker's direct employer?", to='survey.Employer', null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='event',
            field=models.TextField(verbose_name='event description', help_text='Please describe the event in detail.'),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='event_city',
            field=models.CharField(verbose_name='event city', max_length=255, help_text='In what city did the event take place?', blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='event_state',
            field=localflavor.us.models.USPostalCodeField(verbose_name='event state', max_length=2, help_text='In what state did the event take place?', choices=[('AL', 'Alabama'), ('AK', 'Alaska'), ('AS', 'American Samoa'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('AA', 'Armed Forces Americas'), ('AE', 'Armed Forces Europe'), ('AP', 'Armed Forces Pacific'), ('CA', 'California'), ('CO', 'Colorado'), ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FM', 'Federated States of Micronesia'), ('FL', 'Florida'), ('GA', 'Georgia'), ('GU', 'Guam'), ('HI', 'Hawaii'), ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'), ('LA', 'Louisiana'), ('ME', 'Maine'), ('MH', 'Marshall Islands'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'), ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'), ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'), ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('MP', 'Northern Mariana Islands'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'), ('PW', 'Palau'), ('PA', 'Pennsylvania'), ('PR', 'Puerto Rico'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'), ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VI', 'Virgin Islands'), ('VA', 'Virginia'), ('WA', 'Washington'), ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming')], blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='exposure',
            field=models.ForeignKey(verbose_name='exposure', help_text='What risk was the worker exposed to?', to='survey.Exposure', null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='first_name',
            field=models.CharField(verbose_name='first name', max_length=255, help_text="What was the deceased worker's first name?", blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='immigrant',
            field=models.NullBooleanField(verbose_name='immigrant', default=None, help_text='Was the worker an immigrant?'),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='industry',
            field=models.ForeignKey(verbose_name='industry', help_text="Please choose an industry code that best corresponds to the worker's occupation.", to='naics.NAICSCode', null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='last_name',
            field=models.CharField(verbose_name='last name', max_length=255, help_text="What was the deceased worker's last name?", blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='occupation',
            field=models.ForeignKey(verbose_name='occupation', help_text="What was the worker's occupation?", to='survey.Occupation', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='origin_city',
            field=models.CharField(verbose_name='origin city', max_length=255, help_text='What city was the worker originally from?', blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='origin_country',
            field=models.ForeignKey(verbose_name='origin country', help_text='What country was the worker originally from?', to='locality.Country', null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='origin_state',
            field=localflavor.us.models.USPostalCodeField(verbose_name='origin state', max_length=2, help_text='What state was the worker originally from?', choices=[('AL', 'Alabama'), ('AK', 'Alaska'), ('AS', 'American Samoa'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('AA', 'Armed Forces Americas'), ('AE', 'Armed Forces Europe'), ('AP', 'Armed Forces Pacific'), ('CA', 'California'), ('CO', 'Colorado'), ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FM', 'Federated States of Micronesia'), ('FL', 'Florida'), ('GA', 'Georgia'), ('GU', 'Guam'), ('HI', 'Hawaii'), ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'), ('LA', 'Louisiana'), ('ME', 'Maine'), ('MH', 'Marshall Islands'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'), ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'), ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'), ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('MP', 'Northern Mariana Islands'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'), ('PW', 'Palau'), ('PA', 'Pennsylvania'), ('PR', 'Puerto Rico'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'), ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VI', 'Virgin Islands'), ('VA', 'Virginia'), ('WA', 'Washington'), ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming')], blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='photo',
            field=models.ImageField(verbose_name='photo', help_text='If you have one, please include a photo of the worker.', null=True, blank=True, upload_to='images/%Y/%m/%d'),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='sex',
            field=models.CharField(verbose_name='sex', max_length=255, help_text="What was the deceased worker's sex?", choices=[('male', 'male'), ('female', 'female')], blank=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='sources',
            field=models.ManyToManyField(verbose_name='sources', to='survey.Source', help_text='What was the source of your information about this worker?', blank=True),
        ),
    ]
