# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0010_workerfatality_notes'),
    ]

    operations = [
        migrations.AddField(
            model_name='workerfatality',
            name='email',
            field=models.EmailField(help_text='(Optional) Please include an email we can contact if we need further information.', max_length=254, verbose_name='Email', blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='workerfatality',
            name='contracts',
            field=models.ManyToManyField(help_text='Enter the name of the company who contracted with the worker’s direct employer, if applicable', to='survey.Contract', blank=True, verbose_name='Contractor under which worker worked'),
        ),
    ]
