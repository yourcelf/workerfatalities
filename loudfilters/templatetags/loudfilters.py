from django import template
from django.db import models

register = template.Library()

@register.inclusion_tag('loudfilters/loudfilters.html')
def loudfilters(cl):
    filter_links = []
    all_used = []

    # Sidebar filters
    if cl.has_filters:
        for spec in cl.filter_specs:
            if spec.used_parameters:
                all_used += list(spec.used_parameters.keys())
                display = None
                for choice in spec.choices(cl):
                    if choice['selected']:
                        display = choice['display']
                        break
                filter_links.append((
                    cl.get_query_string(remove=list(spec.used_parameters.keys())),
                    spec.title,
                    display
                ))

    # Date hierarchy filter
    if cl.date_hierarchy:
        field_name = cl.date_hierarchy
        field = cl.opts.get_field(field_name)
        dates_or_datetimes = 'datetimes' if isinstance(field, models.DateTimeField) else 'dates'
        year_field = '%s__year' % field_name
        month_field = '%s__month' % field_name
        day_field = '%s__day' % field_name
        field_generic = '%s__' % field_name
        year_lookup = cl.params.get(year_field)
        month_lookup = cl.params.get(month_field)
        day_lookup = cl.params.get(day_field)
        if year_lookup or month_lookup or day_lookup:
            all_used += [year_field, month_field, day_field]
            filter_links.append((
                cl.get_query_string(remove=(year_field, month_field, day_field)),
                field.verbose_name,
                "-".join([a for a in (year_lookup, month_lookup, day_lookup) if a])
            ))

    # search_fields search 
    if cl.params.get("q"):
        all_used.append("q")
        filter_links.append((
            cl.get_query_string(remove="q"),
            "Search",
            cl.params.get("q")))
    return {'cl': cl,
            'filter_links': filter_links,
            'clear_link': cl.get_query_string(remove=all_used)
    }
            
