from django.shortcuts import redirect
from django.conf.urls import include, url
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin

admin.site.site_header = _("Worker fatality database administration")
admin.site.site_title = admin.site.site_header

import survey.views

js_info_dict = {
    'packages': ('survey',)
}

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/spreadsheet/$', survey.views.spreadsheet,
        name="spreadsheet"),
    url(r'^admin/spreadsheet/(?P<pk>\d+)/$', survey.views.edit_spreadsheet,
        name="edit_spreadsheet"),
    url(r'^admin/spreadsheet/(?P<pk>\d+)/update-entry/$', survey.views.update_entry,
        name='update_pending_spreadsheet_entry'),
    url(r'^admin/spreadsheet/(?P<pk>\d+)/saved/$', survey.views.spreadsheet_saved,
        name='spreadsheet_saved'),
    url(r'^admin/check-saved-duplicates/$', survey.views.check_saved_duplicates,
        name='check_saved_duplicates'),
    url(r'^admin/merge-worker-fatalities/$', survey.views.merge_worker_fatalities,
        name='merge_worker_fatalities'),
    url(r'^admin/delete-then-check-saved-duplicates/$',
        survey.views.delete_then_check_saved_duplicates,
        name='delete_then_check_saved_duplicates'),
    url(r'^admin/export.(?P<fmt>(csv|json|xlsx))$', survey.views.export_worker_fatalities,
        name='export_worker_fatalities'),

    url(r'^form/$', survey.views.public_form,
        name='public_form'),
    url(r'^form/thanks/$', survey.views.public_form_thanks,
        name='public_form_thanks'),
    url(r'^duplicates-ajax/$', survey.views.duplicates_ajax,
        name='duplicates_ajax'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^jsi18n/$', 'django.views.i18n.javascript_catalog', js_info_dict,
        name='javascript_i18n'),
    url(r'^$', lambda r: redirect("spreadsheet"))
]
