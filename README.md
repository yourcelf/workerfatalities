# Worker Fatality Database

Django web application for collecting information about workers killed on the
job in the US. Supports deduplication, bulk entry, single public entry, and
spreadsheet exports.


