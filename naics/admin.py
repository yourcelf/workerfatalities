from django.contrib import admin
from naics.models import NAICSCode

class TopLevelListFilter(admin.SimpleListFilter):
    title = 'Top-level'
    parameter_name = 'top_level'
    def lookups(self, request, model_admin):
        return (('top_level', "Top Level Only"),)

    def queryset(self, request, queryset):
        return queryset.filter(ancestor__isnull=True)

# Register your models here.
class NAICSCodeAdmin(admin.ModelAdmin):
    list_display = ['code', 'description_en', 'description_es']
    search_fields = ['code', 'description', 'description_en', 'description_es']
    list_filter = [
        TopLevelListFilter,
        ('ancestor', admin.RelatedOnlyFieldListFilter),
        'edition'
    ]
    readonly_fields = ['code', 'description', 'ancestor', 'edition']

    def has_add_permission(self, *args, **kwargs):
        return False

    def has_delete_permission(self, *args, **kwargs):
        return False

    def changlist_view(self, request, extra_context=None):
        if extra_context is None:
            extra_context = {}
        extra_context['title'] = "Select {} to view".format(self.model._meta.verbose_name)
        return super().changelist_view(
                request, object_id, form_url, extra_context)

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        if extra_context is None:
            extra_context = {}
        extra_context['title'] = "View {}".format(self.model._meta.verbose_name)
        return super().changeform_view(
                request, object_id, form_url, extra_context)

admin.site.register(NAICSCode, NAICSCodeAdmin)

