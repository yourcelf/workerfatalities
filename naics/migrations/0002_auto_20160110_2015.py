# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('naics', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='naicscode',
            name='description_en',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='naicscode',
            name='description_es',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
