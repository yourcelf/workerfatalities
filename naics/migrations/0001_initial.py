# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='NAICSCode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('code', models.CharField(max_length=6)),
                ('description', models.CharField(max_length=255)),
                ('edition', models.CharField(max_length=255)),
                ('ancestor', models.ForeignKey(to='naics.NAICSCode', null=True)),
            ],
            options={
                'verbose_name': 'NAICS Code',
                'ordering': ['code'],
                'verbose_name_plural': 'NAICS Codes',
            },
        ),
    ]
