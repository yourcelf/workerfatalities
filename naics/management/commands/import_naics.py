from django.core.management.base import BaseCommand
import tempfile
import requests
import xlrd
import re

from naics.models import NAICSCode

class Command(BaseCommand):
    url = "http://www.census.gov/eos/www/naics/2012NAICS/2-digit_2012_Codes.xls"
    # spanish via http://www3.inegi.org.mx/sistemas/SCIAN/scian.aspx
    spanish_url = "http://www3.inegi.org.mx/sistemas/scian/doc/SCIAN_2013_estructura_productos.xlsx"
    help = "Import 2012 NAICS codes from census.gov"

    def handle(self, *args, **options):
        ancestors = {}
        spanish = {}

        with tempfile.NamedTemporaryFile(suffix='.xlsx') as fh:
            r = requests.get(self.spanish_url)
            fh.write(r.content)
            fh.flush()

            book = xlrd.open_workbook(fh.name)
            sheet = book.sheet_by_index(0)
            for i in range(sheet.nrows):
                seq = sheet.cell(i, 0)
                try:
                    code = str(int(sheet.cell(i, 0).value))
                except ValueError:
                    continue
                if re.match('^\d+$', code):
                    description = str(sheet.cell(i, 1).value)
                    spanish[code] = description.strip().rstrip("T")


        english_to_spanish = {}

        with tempfile.NamedTemporaryFile(suffix=".xls") as fh:
            r = requests.get(self.url)
            fh.write(r.content)
            fh.flush()

            book = xlrd.open_workbook(fh.name)
            sheet = book.sheet_by_index(0)
            for i in range(sheet.nrows):
                seq = sheet.cell(i, 0)
                if seq.ctype != 2:
                    continue
                code = str(sheet.cell(i, 1).value).split(".")[0]
                description = sheet.cell(i, 2).value.strip()
                description_es = spanish.get(code)
                if description and description_es:
                    english_to_spanish[description] = description_es

            for i in range(sheet.nrows):
                seq = sheet.cell(i, 0)
                if seq.ctype != 2:
                    continue
                code = str(sheet.cell(i, 1).value).split(".")[0]
                description = sheet.cell(i, 2).value.strip()
                description_es = spanish.get(code, english_to_spanish.get(description))

                # Check if it's an ancestor.
                parts = code.split('-')
                if len(parts[0]) == 2:
                    naics, created = NAICSCode.objects.update_or_create(
                            code=code,
                            defaults=dict(
                                description=description,
                                description_en=description,
                                description_es=description_es,
                                edition="2012",
                                ancestor=None))
                    if len(parts) == 1:
                        keys = [parts[0]]
                    else:
                        keys = [str(i) for i in range(int(parts[0]), int(parts[1]) + 1)]
                    for key in keys:
                        ancestors[key] = naics
                else:
                    NAICSCode.objects.update_or_create(code=code,
                            defaults=dict(
                                description=description,
                                description_en=description,
                                description_es=description_es,
                                edition="2012",
                                ancestor=ancestors[code[0:2]]))
