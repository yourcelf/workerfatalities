from django.core.management.base import BaseCommand
from naics.models import NAICSCode

class Command(BaseCommand):
    def handle(self, *args, **options):
        for c in NAICSCode.objects.all():
            print(",".join(['"{}"'.format(a or "") for a in (c.code, c.description_en, c.description_es)]))
