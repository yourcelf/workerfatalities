import os
import subprocess
from django.core.management.base import BaseCommand

from naics.models import NAICSCode

DEST = os.path.join(os.path.dirname(__file__), "..", "..", "fixtures", "naics.json")

class Command(BaseCommand):
    help = "Write naics data fixture."

    def handle(self, *args, **options):
        result = subprocess.check_output([
            "python", "manage.py", "dumpdata", "naics",
            "--indent", "2"])
        with open(DEST, 'w') as fh:
            fh.write(result.decode('utf-8'))
