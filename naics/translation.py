from modeltranslation.translator import translator, TranslationOptions
from naics.models import NAICSCode

class NAICSCodeTranslationOptions(TranslationOptions):
    fields = ('description',)
translator.register(NAICSCode, NAICSCodeTranslationOptions)
