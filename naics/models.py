from django.db import models

class NAICSCodeManager(models.Manager):
    def top_level(self):
        return self.filter(ancestor__isnull=True)

# Create your models here.
class NAICSCode(models.Model):
    code = models.CharField(max_length=6)
    description = models.CharField(max_length=255)
    ancestor = models.ForeignKey('self', null=True)
    edition = models.CharField(max_length=255)

    objects = NAICSCodeManager()

    def __str__(self):
        return "{} - {}".format(self.code, self.description)

    def to_dict(self):
        return {
            "id": self.id,
            "code": self.code,
            "description": self.description,
            "edition": self.edition,
            "ancestor": self.ancestor.id if self.ancestor else None
        }

    def get_parent(self):
        if len(self.code.split("-")[0]) == 2:
            return None
        elif len(self.code) == 3:
            return self.ancestor
        return self.objects.get(code=self.code[:-1])

    class Meta:
        ordering = ['code']
        verbose_name = "NAICS Code"
        verbose_name_plural = "NAICS Codes"
