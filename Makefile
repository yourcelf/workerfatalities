messages:
	venv/bin/python manage.py compress --force
	venv/bin/django-admin makemessages -l en -l es --ignore node_modules --ignore bower_components --ignore venv
	venv/bin/django-admin makemessages -l en -l es -d djangojs --ignore node_modules --ignore bower_components --ignore venv --ignore collected_static/admin --ignore collected_static/bootstrap --ignore collected_static/select2
	venv/bin/django-admin compilemessages
